;-------------------------------------------------------------------------------
;
; Undertale NES Demo
; Constants
;
; Original game by Toby Fox
; This port is written by Cytlan
;
; Copyright 2018 Cytlan
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
; 
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
; 
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
; 
; THIS  SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND  CONTRIBUTORS  "AS IS"
; AND  ANY  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT  LIMITED  TO,  THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  LIABLE
; FOR  ANY  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,  OR  CONSEQUENTIAL
; DAMAGES  (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS  OR
; SERVICES;  LOSS  OF USE, DATA, OR PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  USE
; OF  THIS  SOFTWARE,  EVEN  IF  ADVISED OF  THE  POSSIBILITY  OF  SUCH  DAMAGE.
;
; Created:   2018-10-07
; Modified:  2018-10-14
;-------------------------------------------------------------------------------
.ifndef _CONSTANTS_INC_
_CONSTANTS_INC_ = 1

CPU_PAL = 0
CPU_NTSC = 1

PPU_CTRL   = $2000 ; VPHB SINN  NMI enable (V), PPU master/slave (P), sprite height (H), background tile select (B), sprite tile select (S), increment mode (I), nametable select (NN)
PPU_MASK   = $2001 ; BGRs bMmG  color emphasis (BGR), sprite enable (s), background enable (b), sprite left column enable (M), background left column enable (m), greyscale (G)
PPU_STATUS = $2002 ; VSO- ----  vblank (V), sprite 0 hit (S), sprite overflow (O), read resets write pair for $2005/2006
OAM_ADDR   = $2003 ; aaaa aaaa  OAM read/write address
OAM_DATA   = $2004 ; dddd dddd  OAM data read/write
PPU_SCROLL = $2005 ; xxxx xxxx  fine scroll position (two writes: X, Y)
PPU_ADDR   = $2006 ; aaaa aaaa  PPU read/write address (two writes: MSB, LSB)
PPU_DATA   = $2007 ; dddd dddd  PPU data read/write
OAM_DMA    = $4014 ; aaaa aaaa  OAM DMA high address 

PPU_NMI_ENABLE = $80
PPU_SPR_HEIGHT = $20
PPU_BG_SELECT  = $10
PPU_SPR_SELECT = $08
PPU_INC_MODE   = $04

PPU_BG_ENABLE = $08

; MMC3 registers
MMC3_BANK_SELECT     = $8000
MMC3_BANK_DATA       = $8001
MMC3_MIRRORING       = $A000
MMC3_PRG_RAM_PROTECT = $A001

.endif