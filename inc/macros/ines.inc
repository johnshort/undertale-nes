;-------------------------------------------------------------------------------
;
; INES Header Generation
;
; Author: Cytlan
; Date:   2018-10-06
;-------------------------------------------------------------------------------

.macro ines_make_header
	; Get ROM size
	.if .defined(INES_ROM_SIZE)
		.if (INES_ROM_SIZE > 67108863)
			.fatal "Invalid constant: INES_CHR_SIZE: Can only be 0 to 67108863"
			.fatal "ROM size may not exceed 67108863 bytes"
		.endif
		INES_ROM_SIZE_ .set (INES_ROM_SIZE >> 15)
		.if (INES_ROM_SIZE - (INES_ROM_SIZE_ << 15)) > 0
			INES_ROM_SIZE_ .set INES_ROM_SIZE_ + 1
		.endif
	.else
		.fatal "Missing required constant: INES_ROM_SIZE"
	.endif

	; Get CHR size
	.if .defined(INES_CHR_SIZE)
		.if (INES_CHR_SIZE > 33554432)
			.fatal "Invalid constant: INES_CHR_SIZE: Can only be 0 to 33554432"
		.endif
		INES_CHR_SIZE_ .set (INES_CHR_SIZE >> 13)
		.if (INES_CHR_SIZE - (INES_CHR_SIZE_ << 13)) > 0
			INES_CHR_SIZE_ .set INES_CHR_SIZE_ + 1
		.endif
	.else
		.fatal "Missing required constant: INES_CHR_SIZE"
	.endif

	; Get mapper
	.if .defined(INES_MAPPER)
		.if (INES_MAPPER > 67108863)
			.fatal "Invalid constant: INES_MAPPER: Can only be 0 to 4095"
		.endif	
	.else
		.fatal "Missing required constant: INES_CHR_SIZE"
	.endif

	; Get submapper
	.if .defined(INES_SUBMAPPER)
		.if (INES_SUBMAPPER > 15)
			.fatal "Invalid constant: INES_SUBMAPPER: Can only be 0 to 15"
		.endif
		INES_SUBMAPPER_ .set INES_MIRRORING
	.else
		INES_SUBMAPPER_ .set 0
	.endif

	; Get mirroring
	.if .defined(INES_MIRRORING)
		.if (INES_MIRRORING > 1)
			.fatal "Invalid constant: INES_MIRRORING: Can only be 1 or 0"
		.endif
		INES_MIRRORING_ .set INES_MIRRORING
	.else
		INES_MIRRORING_ .set 0
	.endif

	; Get bettery backed ram setting
	.if .defined(INES_BATTERY_BACKED_RAM)
		.if (INES_BATTERY_BACKED_RAM > 1)
			.fatal "Invalid constant: INES_BATTERY_BACKED_RAM: Can only be 1 or 0"
		.endif
		INES_BATTERY_BACKED_RAM_ .set INES_BATTERY_BACKED_RAM
	.else
		INES_BATTERY_BACKED_RAM_ .set 0
	.endif

	; Get trainer setting
	.if .defined(INES_TRAINER)
		.if (INES_TRAINER > 1)
			.fatal "Invalid constant: INES_TRAINER: Can only be 1 or 0"
		.endif
		INES_TRAINER_ .set INES_TRAINER
	.else
		INES_TRAINER_ .set 0
	.endif

	; Get four-screen vram setting
	.if .defined(INES_FOUR_SCREEN)
		.if (INES_FOUR_SCREEN > 1)
			.fatal "Invalid constant: INES_FOUR_SCREEN: Can only be 1 or 0"
		.endif
		INES_FOUR_SCREEN_ .set INES_FOUR_SCREEN
	.else
		INES_FOUR_SCREEN_ .set 0
	.endif

	; Get VS Unisystem setting
	.if .defined(INES_VS_UNISYSTEM)
		.if (INES_VS_UNISYSTEM > 1)
			.fatal "Invalid constant: INES_VS_UNISYSTEM: Can only be 1 or 0"
		.endif
		INES_VS_UNISYSTEM_ .set INES_VS_UNISYSTEM
	.else
		INES_VS_UNISYSTEM_ .set 0
	.endif

	; Get PlayChoice-10 setting
	.if .defined(INES_PLAYCHOICE)
		.if (INES_PLAYCHOICE > 1)
			.fatal "Invalid constant: INES_VS_UNISYSTEM: Can only be 1 or 0"
		.endif
		INES_PLAYCHOICE_ .set INES_PLAYCHOICE
	.else
		INES_PLAYCHOICE_ .set 0
	.endif

	; Get PlayChoice-10 setting
	.if .defined(INES_TV_SYSTEM)
		.if (INES_TV_SYSTEM > 1)
			.fatal "Invalid constant: INES_TV_SYSTEM: Can only be 1 (PAL) or 0 (NTSC)"
		.endif
		INES_TV_SYSTEM_ .set INES_TV_SYSTEM
	.else
		INES_TV_SYSTEM_ .set 0
	.endif

	; Byte 0-3 - NES identification magic	
	.byte "NES", $1a

	; Byte 4 - PRG ROM size  (16 384 bytes)
	.byte (INES_ROM_SIZE_ & $FF)
	
	; Byte 5 - CHR ROM size
	.byte (INES_CHR_SIZE_ & $FF)

	; Byte 6 - Flags 6
	.byte ((INES_MAPPER & $F) << 4) | (INES_FOUR_SCREEN_ << 3) | (INES_TRAINER_ << 2) | (INES_BATTERY_BACKED_RAM_ << 1) | INES_MIRRORING_

	; Byte 7 - Flags 7 (NES 2.0 - Bits 2 and 3 must be 10 to identify NES 2.0)
	.byte (((INES_MAPPER >> 4) & $F) << 4) | (1 << 3) | (0 << 2) | (INES_PLAYCHOICE_ << 1) | INES_VS_UNISYSTEM_

	; Byte 8 - Mapper variant (NES 2.0)
	.byte (INES_SUBMAPPER_ << 4) | ((INES_MAPPER >> 8) & $F)

	; Byte 9 - Extended ROM (NES 2.0)
	.byte (((INES_ROM_SIZE_ >> 8) & $F) << 4) | ((INES_CHR_SIZE_ >> 8) & $F)
	
	; Byte 10 - PRG RAM (NES 2.0)
	.byte $00
	
	; Byte 11 - CHR RAM (NES 2.0)
	.byte $07
	
	; Byte 12 - TV system (NES 2.0)
	.byte $00
	
	; Byte 13 - VS hardware (NES 2.0)
	.byte $00
	
	; Reserved - Do not use
	.byte $00
	
	; Reserved - Do not use
	.byte $00
	
.endmacro
