;-------------------------------------------------------------------------------
;
; Banking macros
;
; Author: Cytlan
; Date:   2018-12-13
;-------------------------------------------------------------------------------

.macro jsrbank addr, bank
	lda #bank  ; Bank #
	ldx #>addr ; Routine to jump to
	ldy #<addr
	jsr JumpToBankSubroutine
.endmacro
