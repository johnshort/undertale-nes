;-------------------------------------------------------------------------------
;
; Undertale NES Demo
; Sound Engine macros
;
; Original game by Toby Fox
; This port is written by Cytlan
;
; Copyright 2018 Cytlan
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
; 
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
; 
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
; 
; THIS  SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND  CONTRIBUTORS  "AS IS"
; AND  ANY  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT  LIMITED  TO,  THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  LIABLE
; FOR  ANY  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,  OR  CONSEQUENTIAL
; DAMAGES  (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS  OR
; SERVICES;  LOSS  OF USE, DATA, OR PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  USE
; OF  THIS  SOFTWARE,  EVEN  IF  ADVISED OF  THE  POSSIBILITY  OF  SUCH  DAMAGE.
;
; Created:   2018-10-07
; Modified:  2018-10-13
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;
; Flow control
;
;-------------------------------------------------------------------------------

.macro S_TIME t
	.if t = 0
		.byte 0, 0
	.else
		T .set t / CPU_MS_PER_FRAME
		.byte <T, >T
	.endif
.endmacro

.macro S_END
	.byte $00
.endmacro

.macro S_RESET_TIMER
	.byte $0E
.endmacro

.macro S_GOTO l
	.byte $0F, <l, >l
.endmacro

;-------------------------------------------------------------------------------
;
; Square 1
;
;-------------------------------------------------------------------------------

.macro S_SQ1_PERIOD freq
	P .set S_FREQ_TO_PERIOD freq
	.byte $01, <P, (>P & $F)
.endmacro

.macro S_SQ1_NOTE note
	P .set S_NOTE_TO_PERIOD note
	.byte $01, <P, (>P & $F)
.endmacro

.macro S_SQ1_VOLUME v
	.byte $05, (v & $F)
.endmacro

.macro S_SQ1_MACRO_VOLUME v
	.byte $18, (v & $F)
.endmacro

.macro S_SQ1_CONFIG c
	.byte $09, c
.endmacro

.macro S_SQ1_DUTY c
	.byte $09, (c << 6) | $30
.endmacro

;-------------------------------------------------------------------------------
;
; Square 2
;
;-------------------------------------------------------------------------------

.macro S_SQ2_PERIOD freq
	P .set S_FREQ_TO_PERIOD freq
	.byte $02, <P, (>P & $F)
.endmacro

.macro S_SQ2_NOTE note
	P .set S_NOTE_TO_PERIOD note
	.byte $02, <P, (>P & $F)
.endmacro

.macro S_SQ2_VOLUME v
	.byte $06, (v & $F)
.endmacro

.macro S_SQ2_MACRO_VOLUME v
	.byte $19, (v & $F)
.endmacro

.macro S_SQ2_CONFIG c
	.byte $0A, c
.endmacro

.macro S_SQ2_DUTY c
	.byte $0A, (c << 6) | $30
.endmacro

;-------------------------------------------------------------------------------
;
; Triangle
;
;-------------------------------------------------------------------------------

.macro S_TRIANGLE_PERIOD freq
	P .set S_FREQ_TO_PERIOD freq
	.byte $03, <P, (>P & $F)
.endmacro

.macro S_TRIANGLE_NOTE note
	P .set S_NOTE_TO_PERIOD note
	.byte $03, <P, (>P & $F)
.endmacro

.macro S_TRIANGLE_VOLUME v
	.byte $07, (v & $F)
.endmacro

.macro S_TRIANGLE_CONFIG c
	.byte $0B, c
.endmacro

;-------------------------------------------------------------------------------
;
; Noise
;
;-------------------------------------------------------------------------------

.macro S_NOISE_PERIOD freq
	.byte $04, freq
.endmacro

.macro S_NOISE_NOTE note
	.byte $04, note
.endmacro

.macro S_NOISE_VOLUME v
	.byte $08, (v & $F)
.endmacro

.macro S_NOISE_MACRO_VOLUME v
	.byte $1B, (v & $F)
.endmacro

.macro S_NOISE_CONFIG c
	.byte $0C, c
.endmacro

.macro S_NOISE_DUTY c
	.byte $0C, (c << 6) | $30
.endmacro

;-------------------------------------------------------------------------------
;
; Effects
;
;-------------------------------------------------------------------------------

.macro S_SQ1_SLIDEUP n
	.byte $30, n
.endmacro

.macro S_SQ1_SLIDEDOWN n
	.byte $34, n
.endmacro

.macro S_SQ2_SLIDEUP n
	.byte $31, n
.endmacro

.macro S_SQ2_SLIDEDOWN n
	.byte $35, n
.endmacro

.macro S_TRIANGLE_SLIDEUP n
	.byte $32, n
.endmacro

.macro S_TRIANGLE_SLIDEDOWN n
	.byte $36, n
.endmacro

.macro S_NOISE_SLIDEUP n
	.byte $33, n
.endmacro

.macro S_NOISE_SLIDEDOWN n
	.byte $37, n
.endmacro

;-------------------------------------------------------------------------------
;
; Macro control
;
;-------------------------------------------------------------------------------

.macro S_SET_MACRO n, l
	.byte ($10 + n), <l, >l
.endmacro

.macro S_RESET_MACRO n
	.byte ($20 + n)
.endmacro

;-------------------------------------------------------------------------------
;
; Frequencies and periods
;
;-------------------------------------------------------------------------------

.define S_FREQ_TO_PERIOD(freq) (CPU_FREQUENCY/(16*freq))-1

.define S_NOTE_TO_PERIOD(freq) (CPU_FREQUENCY/freq)

; These frequencies are the note frequency * 16
S_0   = 1
S_C0  = 262
S_Cs0 = 277
S_D0  = 294
S_Ds0 = 311
S_E0  = 330
S_F0  = 349
S_Fs0 = 370
S_G0  = 392
S_Gs0 = 415
S_A0  = 440
S_As0 = 466
S_B0  = 494
S_C1  = 523
S_Cs1 = 554
S_D1  = 587
S_Ds1 = 622
S_E1  = 659
S_F1  = 698
S_Fs1 = 740
S_G1  = 784
S_Gs1 = 831
S_A1  = 880
S_As1 = 932
S_B1  = 988
S_C2  = 1047
S_Cs2 = 1109
S_D2  = 1175
S_Ds2 = 1244
S_E2  = 1319
S_F2  = 1397
S_Fs2 = 1480
S_G2  = 1568
S_Gs2 = 1661
S_A2  = 1760
S_As2 = 1865
S_B2  = 1976
S_C3  = 2093
S_Cs3 = 2217
S_D3  = 2349
S_Ds3 = 2489
S_E3  = 2637
S_F3  = 2794
S_Fs3 = 2960
S_G3  = 3136
S_Gs3 = 3322
S_A3  = 3520
S_As3 = 3729
S_B3  = 3951
S_C4  = 4186
S_Cs4 = 4435
S_D4  = 4699
S_Ds4 = 4978
S_E4  = 5274
S_F4  = 5588
S_Fs4 = 5920
S_G4  = 6272
S_Gs4 = 6645
S_A4  = 7040
S_As4 = 7459
S_B4  = 7902
S_C5  = 8372
S_Cs5 = 8870
S_D5  = 9397
S_Ds5 = 9956
S_E5  = 10548
S_F5  = 11175
S_Fs5 = 11840
S_G5  = 12544
S_Gs5 = 13290
S_A5  = 14080
S_As5 = 14917
S_B5  = 15804
S_C6  = 16744
S_Cs6 = 17740
S_D6  = 18795
S_Ds6 = 19912
S_E6  = 21096
S_F6  = 22351
S_Fs6 = 23680
S_G6  = 25088
S_Gs6 = 26580
S_A6  = 28160
S_As6 = 29835
S_B6  = 31608
S_C7  = 33488
S_Cs7 = 35479
S_D7  = 37589
S_Ds7 = 39824
S_E7  = 42192
S_F7  = 44701
S_Fs7 = 47359
S_G7  = 50175
S_Gs7 = 53159
S_A7  = 56320
S_As7 = 59669
S_B7  = 63217
S_C8  = 66976
S_Cs8 = 70959
S_D8  = 75178
S_Ds8 = 79648
S_E8  = 84385
S_F8  = 89402
S_Fs8 = 94719
S_G8  = 100351
S_Gs8 = 106318
S_A8  = 112640
S_As8 = 119338
S_B8  = 126434

S_N0 = 0
S_N1 = 1
S_N2 = 2
S_N3 = 3
S_N4 = 4
S_N5 = 5
S_N6 = 6
S_N7 = 7
S_N8 = 8
S_N9 = 9
S_NA = 10
S_NB = 11
S_NC = 12
S_ND = 13
S_NE = 14
S_NF = 15

