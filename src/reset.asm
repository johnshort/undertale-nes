;-------------------------------------------------------------------------------
;
; Undertale NES Demo
; Reset routine
;
; Original game by Toby Fox
; This port is written by Cytlan
;
; Copyright 2018 Cytlan
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
; 
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
; 
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
; 
; THIS  SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND  CONTRIBUTORS  "AS IS"
; AND  ANY  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT  LIMITED  TO,  THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  LIABLE
; FOR  ANY  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,  OR  CONSEQUENTIAL
; DAMAGES  (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS  OR
; SERVICES;  LOSS  OF USE, DATA, OR PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  USE
; OF  THIS  SOFTWARE,  EVEN  IF  ADVISED OF  THE  POSSIBILITY  OF  SUCH  DAMAGE.
;
; Created:   2018-10-25
; Modified:  2018-12-13
;-------------------------------------------------------------------------------

.include "options.inc"
.include "macros.inc"

.autoimport +

.segment "Reset"

; Reset routine
.proc Reset
	; Set up registers
	sei      ; Disable IRQ interrupts
	cld      ; The NES doesn't have decimal mode, so just disable it
	ldx #$FF ;
	txs      ; Stack must be initialized to $FF
	inx      ; Make x 0 (By overflowing)
	txa      ; Make a 0
	tay      ; Make y 0

	; Enable & Protect PRG RAM
	lda #$C0
	sta MMC3_PRG_RAM_PROTECT

	; We'll only be using vertical mirroring, so might as well set that here.
	lda #$00
	sta MMC3_MIRRORING

	; Need to initialize banking
	jsr BankingInitialize

	; Jump to utilities bank and initialize
	jsrbank Initialize, 0

	; Infinite loop
	:
	jmp :-
.endproc

.export Reset
