;-------------------------------------------------------------------------------
;
; Undertale NES Demo
; Sound Engine
; Bank 2
;
; Original game by Toby Fox
; This port is written by Cytlan
;
; Copyright 2018 Cytlan
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
; 
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
; 
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
; 
; THIS  SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND  CONTRIBUTORS  "AS IS"
; AND  ANY  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT  LIMITED  TO,  THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  LIABLE
; FOR  ANY  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,  OR  CONSEQUENTIAL
; DAMAGES  (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS  OR
; SERVICES;  LOSS  OF USE, DATA, OR PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  USE
; OF  THIS  SOFTWARE,  EVEN  IF  ADVISED OF  THE  POSSIBILITY  OF  SUCH  DAMAGE.
;
; Created:   2018-10-07
; Modified:  2018-10-14
;-------------------------------------------------------------------------------

.include "options.inc"
.include "macros.inc"

;-------------------------------------------------------------------------------
;
; Memory
;
;-------------------------------------------------------------------------------

; High priority RAM
.segment "SoundZP"
.zeropage

	SoundState:          .res 1 ; 0: Stopped, 1: Playing
	SoundTimer:                 ; Frame counter
	SoundTimerLo:        .res 1
	SoundTimerHi:        .res 1

	; Main music pointer
	SoundMusicPtr:
	SoundMusicPtrHi:     .res 1
	SoundMusicPtrLo:     .res 1

	; Interpreter frame pointer
	SoundFramePtr:
	SoundFramePtrLo:     .res 1
	SoundFramePtrHi:     .res 1

	; Channel update flags
	SoundUpdateSq1:      .res 1
	SoundUpdateSq1Upper: .res 1
	SoundUpdateSq2:      .res 1
	SoundUpdateSq2Upper: .res 1
	SoundUpdateTriangle: .res 1
	SoundUpdateNoise:    .res 1

	; Square 1
	SoundSq1Config:      .res 1 ; For duty cycle and channel volume
	SoundSq1Volume:      .res 1 ; Macro volume
	SoundSq1Freq:
	SoundSq1FreqLo:      .res 1
	SoundSq1FreqHi:      .res 1

	; Square 2
	SoundSq2Config:      .res 1
	SoundSq2Volume:      .res 1
	SoundSq2Freq:
	SoundSq2FreqLo:      .res 1
	SoundSq2FreqHi:      .res 1

	; Triangle
	SoundTriConfig:      .res 1
	SoundTriFreq:
	SoundTriFreqLo:      .res 1
	SoundTriFreqHi:      .res 1

	; Noise
	SoundNoiseConfig:    .res 1
	SoundNoiseVolume:    .res 1
	SoundNoiseFreq:
	SoundNoiseFreqLo:    .res 1
	SoundNoiseFreqHi:    .res 1

	; Macro state
	SoundMacrosEnabled:  .res 1
	SoundMacrosShift:    .res 1
	SoundMacrosCount:    .res 1

; Low priority RAM
.segment "SoundRAM"

	; Macro pointers
	SoundMacroPointers:
	SoundMacro0Ptr:      .res 2
	SoundMacro1Ptr:      .res 2
	SoundMacro2Ptr:      .res 2
	SoundMacro3Ptr:      .res 2
	SoundMacro4Ptr:      .res 2
	SoundMacro5Ptr:      .res 2
	SoundMacro6Ptr:      .res 2
	SoundMacro7Ptr:      .res 2

	; Base pointers (Regular pointers are loaded from these on reset)
	SoundMacroBasePointers:
	SoundMacro0PtrBase:  .res 2
	SoundMacro1PtrBase:  .res 2
	SoundMacro2PtrBase:  .res 2
	SoundMacro3PtrBase:  .res 2
	SoundMacro4PtrBase:  .res 2
	SoundMacro5PtrBase:  .res 2
	SoundMacro6PtrBase:  .res 2
	SoundMacro7PtrBase:  .res 2

.segment "SoundCode"

;-------------------------------------------------------------------------------
;
; Interface routines
;
;-------------------------------------------------------------------------------

; Clear all state memory
.proc SoundInit
	ndxLuaExecStr "print('Initializing sound')"

	;.ifdef DEBUG
		ndxLuaExecFile "sound_engine.lua"
	;.endif

	; Reset all state memory
	lda #0
	sta SoundState
	sta SoundTimer
	sta SoundTimer + 1
	sta SoundFramePtr
	sta SoundFramePtr + 1
	sta SoundMusicPtr
	sta SoundMusicPtr + 1

	; Reset macro state
	; (It's not nessecary to clear the macro pointers)
	sta SoundMacrosEnabled
	sta SoundMacrosShift

	; Clear sound channels
	sta SoundSq1Config
	sta SoundSq1FreqLo
	sta SoundSq1FreqHi

	sta SoundSq2Config
	sta SoundSq2FreqLo
	sta SoundSq2FreqHi

	sta SoundTriConfig
	sta SoundTriFreq
	sta SoundTriFreq + 1

	sta SoundNoiseConfig
	sta SoundNoiseFreq
	sta SoundNoiseFreq + 1

	; Set default macro volume to 15
	; (Macro volume is subtracted from 15, to make later calculations faster)
	lda #$00
	sta SoundSq1Volume
	sta SoundSq2Volume
	sta SoundNoiseVolume

	; Load the high frequency memory of the square channels with some nonsense.
	; This is so that the sound routine can properly reset the channels on the
	; first iteration.
	lda #$FF
	sta SoundSq1FreqHi + 1
	sta SoundSq2FreqHi + 1

	rts
.endproc

; Set the music track to play
.proc SoundLoadTrack
	sta SoundMusicPtr + 1
	stx SoundMusicPtr
	rts
.endproc

; Enable playback
.proc SoundPlay
	lda #1
	sta SoundState

	; Enable all channels
	lda #$0F
	sta $4015

	rts
.endproc

.proc SoundStop
	lda #0
	sta SoundState
	rts
.endproc

;
; Sound effects are just glorified macros.
; Because macros can do as much or as little as they want in a single step,
; and because sound effects are relatively short, it's a nice space reducting
; method to combine the two.
; Sound effects are always loaded into the highest priority macro, which
; overrides all changes done by any previous operation.
;
.proc SoundPlaySoundEffect
	; Load pointer
	; Enable a new macro
	rts
.endproc

;-------------------------------------------------------------------------------
;
; Tick routine
;
;-------------------------------------------------------------------------------

; Call once every frame
.proc SoundUpdate
	; Just return if state is stopped
	lda SoundState
	bne :+
		rts
	:

	; Update timer
	inc SoundTimer
	bne :+
		inc SoundTimer + 1
	:

	; Keep track of which channels need updating
	lda #$00
	sta SoundUpdateSq1
	sta SoundUpdateSq2
	sta SoundUpdateTriangle
	sta SoundUpdateNoise

	; Check if it's time to run the next set of instructions
	ldy #1
	lda (SoundMusicPtr),y
	cmp SoundTimer + 1
	bmi :+ ; Upper byte is smaller, must update
	bne MusicEnd ; Upper byte is greater, skip
		dey
		lda (SoundMusicPtr),y
		cmp SoundTimer
		bpl MusicEnd
		:
			jmp SoundUpdateMusic
	MusicEnd:

	; Apply macros
	lda #0
	tax
	sta SoundMacrosCount
	lda SoundMacrosEnabled
	sta SoundMacrosShift
	MacroLoop:
	lsr SoundMacrosShift
	bcs MacroUpdate ; If carry set, update macro
	beq MacroEnd    ; If zero, no macros left to update
	bcc MacroSkip   ; If carry clear, don't update this macro
		MacroUpdate:
			; Set pointer
			stx SoundMacrosCount
			lda SoundMacroPointers,x
			sta SoundFramePtr
			inx
			lda SoundMacroPointers,x
			sta SoundFramePtr + 1
			
			; Do the interpreting
			jsr SoundInterpret

			; Update the pointer
			ldx SoundMacrosCount
			lda SoundFramePtr
			sta SoundMacroPointers,x
			inx
			lda SoundFramePtr + 1
			sta SoundMacroPointers,x
			inx
			stx SoundMacrosCount
			jmp MacroLoop
		MacroSkip:
		inx
		inx
		jmp MacroLoop
	MacroEnd:

	; Square 1
	lda SoundUpdateSq1
	beq :++
		lda SoundSq1Config
		sta $4000
		;lda #$00 ;SoundSq2Sweep
		;sta $4001

		; Update upper bits of period?
		lda SoundUpdateSq1Upper
		beq :+
			; Sweep hack?
			;              ; increment timer period's high 3 bits
			;lda #$40      ; reset frame counter in case it was about to clock
			;sta $4017
			;lda #$FF      ; be sure low 8 bits of timer period are $FF
			;sta $4002
			;lda #$87      ; sweep enabled, shift = 7 (1/128)
			;sta $4001
			;lda #$C0      ; clock sweep immediately
			;sta $4017
			;lda #$0F      ; disable sweep
			;sta $4001
			lda #$00
			sta SoundUpdateSq1Upper
			lda SoundSq1FreqHi
			sta $4003
		:
		lda SoundSq1Freq
		sta $4002
	:

	; Square 2
	lda SoundUpdateSq2
	beq :++
		lda SoundSq2Config
		sta $4004
		;lda #$00 ;SoundSq2Sweep
		;sta $4005

		; Update upper bits of period?
		lda SoundUpdateSq2Upper
		beq :+
			lda #$00
			sta SoundUpdateSq2Upper
			lda SoundSq2FreqHi
			sta $4007
		:
		lda SoundSq2Freq
		sta $4006
	:

	; Triangle
	lda SoundUpdateTriangle
	beq :+
		lda SoundTriConfig
		sta $4008
		lda SoundTriFreqLo
		sta $400A
		lda SoundTriFreqHi
		sta $400B
	:

	; Noise
	lda SoundUpdateNoise
	beq :+
		lda SoundNoiseConfig
		sta $400C
		lda SoundNoiseFreqLo
		sta $400E
		lda SoundNoiseFreqHi
		sta $400F
	:

	rts
.endproc

;-------------------------------------------------------------------------------
;
; Update music
;
;-------------------------------------------------------------------------------

.proc SoundUpdateMusic
	; Increment frame pointer by 2, so that we don't interpret the time value as
	; an instruction
	clc
	lda SoundMusicPtr
	adc #$02
	sta SoundMusicPtr
	sta SoundFramePtr
	lda SoundMusicPtr + 1
	adc #$00
	sta SoundMusicPtr + 1
	sta SoundFramePtr + 1

	; Do update
	jsr SoundInterpret

	; Save the frame pointer
	lda SoundFramePtr
	sta SoundMusicPtr
	lda SoundFramePtr + 1
	sta SoundMusicPtr + 1

	jmp SoundUpdate::MusicEnd
.endproc

;-------------------------------------------------------------------------------
;
; Update routine
;
;-------------------------------------------------------------------------------

.proc SoundInterpret
	; Read 1 byte and jump to the appropriate routine
	ldy #$00
	loop:
		; Read opcode from the frame pointer
		lda (SoundFramePtr),y
		iny
		tax

		; Do an RTS trick to jump to the instruction routine as pointed to
		; in the jump table.
		; Push upper "return" address
		lda SoundJumpTableHi,x
		pha
		; Push lower "return" address
		lda SoundJumpTableLo,x
		pha

		; "Return"
		rts
	end:

	; Add Y pointer to the frame pointer
	; Hopefully no frame will contain more than 256 bytes....
	; Else we won't escape...
	; We will never continue...
	; We'll never grow...
	; Oh the humanity, if a frame is greater than 256 bytes...
	tya
	clc
	adc SoundFramePtr
	sta SoundFramePtr
	bcc :+
		inc SoundFramePtr + 1
	:
	rts
.endproc

;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------
;
; Sound Engine Instructions
;
;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;
; Period
;
;-------------------------------------------------------------------------------

; $01
.proc SoundLoadSQ1Period
	; Must update square 1
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateSq1

	; Load lower byte of period
	lda (SoundFramePtr),y
	iny
	sta SoundSq1FreqLo

	; Load upper byte of period
	lda (SoundFramePtr),y
	iny

	; Is new upper not equal to old upper?
	cmp SoundSq1FreqHi
	beq :+
		; Must update sq1 upper nibble
		sta SoundSq1FreqHi
		sty SoundUpdateSq1Upper
	:

	; Return
	jmp SoundInterpret::loop
.endproc

; $02
.proc SoundLoadSQ2Period
	; Must update square 2
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateSq2

	; Load lower byte of period
	lda (SoundFramePtr),y
	iny
	sta SoundSq2FreqLo

	; Load upper byte of period
	lda (SoundFramePtr),y
	iny

	; Is new upper not equal to old upper?
	cmp SoundSq2FreqHi
	beq :+
		; Must update sq2 upper nibble
		sta SoundSq2FreqHi
		sty SoundUpdateSq2Upper
	:

	; Return
	jmp SoundInterpret::loop
.endproc

; $03
.proc SoundLoadTrianglePeriod
	; Must update triangle
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateTriangle

	; Load period
	lda (SoundFramePtr),y
	iny
	sta SoundTriFreqLo
	lda (SoundFramePtr),y
	iny
	sta SoundTriFreqHi

	; Return
	jmp SoundInterpret::loop
.endproc

; $04
.proc SoundLoadNoisePeriod
	; Must update noise
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateNoise

	; Load period
	lda (SoundFramePtr),y
	iny
	sta SoundNoiseFreq

	; Return
	jmp SoundInterpret::loop
.endproc

;-------------------------------------------------------------------------------
;
; Volume
;
;-------------------------------------------------------------------------------

; $05
.proc SoundLoadSQ1Volume
	; Must update sq1
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateSq1

	; Mask old volume
	lda #$F0
	and SoundSq1Config
	sta SoundSq1Config

	; Get new volume
	lda (SoundFramePtr),y
	iny

	; Add new volume
	ora SoundSq1Config
	sta SoundSq1Config

	; Store the value to subtract from any macro volume
	and #$0F
	eor #$0F
	sta SoundSq1Volume

	; Return
	jmp SoundInterpret::loop
.endproc

; $06
.proc SoundLoadSQ2Volume
	; Must update sq2
	sty SoundUpdateSq2

	; Mask old volume
	lda #$F0
	and SoundSq2Config
	sta SoundSq2Config

	; Get new volume
	lda (SoundFramePtr),y
	iny

	; Add new volume
	ora SoundSq2Config
	sta SoundSq2Config

	; Store the value to subtract from any macro volume
	and #$0F
	eor #$0F
	sta SoundSq2Volume

	; Return
	jmp SoundInterpret::loop
.endproc

; $07
.proc SoundLoadTriangleVolume
	; Triangle doesn't have volume
	jmp SoundInterpret::loop
.endproc

; $08
.proc SoundLoadNoiseVolume
	; Must update noise
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateNoise

	; Mask old volume
	lda #$F0
	and SoundNoiseConfig
	sta SoundNoiseConfig

	; Get new volume
	lda (SoundFramePtr),y
	iny
	tax

	; Add new volume
	ora SoundNoiseConfig
	sta SoundNoiseConfig

	; Store new channel volume
	txa
	sec
	sbc #15
	sta SoundNoiseVolume

	; Return
	jmp SoundInterpret::loop
.endproc

;-------------------------------------------------------------------------------
;
; Pitch slide
;
;-------------------------------------------------------------------------------

; TODO: Implement this later......
.proc SoundSq1PitchSlide
	jmp SoundInterpret::loop
.endproc

;-------------------------------------------------------------------------------
;
; Config
;
;-------------------------------------------------------------------------------

; $09
.proc SoundLoadSQ1Config
	; Must update sq1
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateSq1

	; Mask old config
	lda SoundSq1Config
	and #$0F
	sta SoundSq1Config

	; Get config
	lda (SoundFramePtr),y
	iny

	; Add new config
	ora SoundSq1Config
	sta SoundSq1Config

	; Return
	jmp SoundInterpret::loop
.endproc

; $0A
.proc SoundLoadSQ2Config
	; Must update sq2
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateSq2

	; Mask old config
	lda SoundSq2Config
	and #$0F
	sta SoundSq2Config

	; Get config
	lda (SoundFramePtr),y
	iny

	; Add new config
	ora SoundSq2Config
	sta SoundSq2Config

	; Return
	jmp SoundInterpret::loop
.endproc

; $0B
.proc SoundLoadTriangleConfig
	; Must update triangle
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateTriangle

	; Get config
	lda (SoundFramePtr),y
	iny
	sta SoundTriConfig

	; Return
	jmp SoundInterpret::loop
.endproc

; $0C
.proc SoundLoadNoiseConfig
	; Must update noise
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateNoise

	; Mask old config
	lda SoundNoiseConfig
	and #$0F
	sta SoundNoiseConfig

	; Get config
	lda (SoundFramePtr),y
	iny

	; Add new config
	ora SoundNoiseConfig
	sta SoundNoiseConfig

	; Return
	jmp SoundInterpret::loop
.endproc

;-------------------------------------------------------------------------------
;
; Control
;
;-------------------------------------------------------------------------------

; $0E
.proc SoundResetTimer
	lda #$00
	sta SoundTimer
	sta SoundTimer + 1
	;DEBUG_EXEC "print(string.format('S_RESET_TIMER'))"
	jmp SoundInterpret::loop
.endproc

; $0F
.proc SoundGoto
	; Get new pointer
	lda (SoundFramePtr),y
	iny
	pha
	lda (SoundFramePtr),y
	iny
	sta SoundFramePtr + 1
	pla
	sta SoundFramePtr

	; Reset Y index
	ldy #$00

	; Return
	jmp SoundInterpret::end
.endproc

;-------------------------------------------------------------------------------
;
; Set macro pointer & enable / disable
;
;-------------------------------------------------------------------------------

.macro SoundSetMacroX mask, ptr, baseptr
	; Get macro pointer
	lda (SoundFramePtr),y
	iny
	sta baseptr
	sta ptr
	lda (SoundFramePtr),y
	iny
	sta baseptr + 1
	sta ptr + 1

	; Is it null?
	lda baseptr
	bne :+
		lda baseptr + 1
		bne :+
			; Yep, it's null. Disable this macro.
			lda #mask
			and SoundMacrosEnabled
			sta SoundMacrosEnabled
			jmp SoundInterpret::loop
	:
	; It's not null. Enable this macro.
	lda #mask
	ora SoundMacrosEnabled
	sta SoundMacrosEnabled

	jmp SoundInterpret::loop
.endmacro

; $10
.proc SoundSetMacro0
	SoundSetMacroX $01, SoundMacro0Ptr, SoundMacro0PtrBase
.endproc
; $11
.proc SoundSetMacro1
	SoundSetMacroX $02, SoundMacro1Ptr, SoundMacro1PtrBase
.endproc
; $12
.proc SoundSetMacro2
	SoundSetMacroX $04, SoundMacro2Ptr, SoundMacro2PtrBase
.endproc
; $13
.proc SoundSetMacro3
	SoundSetMacroX $08, SoundMacro3Ptr, SoundMacro3PtrBase
.endproc
; $14
.proc SoundSetMacro4
	SoundSetMacroX $10, SoundMacro4Ptr, SoundMacro4PtrBase
.endproc
; $15
.proc SoundSetMacro5
	SoundSetMacroX $20, SoundMacro5Ptr, SoundMacro5PtrBase
.endproc
; $16
.proc SoundSetMacro6
	SoundSetMacroX $40, SoundMacro6Ptr, SoundMacro6PtrBase
.endproc
; $17
.proc SoundSetMacro7
	SoundSetMacroX $80, SoundMacro7Ptr, SoundMacro7PtrBase
.endproc

;-------------------------------------------------------------------------------
;
; Macro Volume
;
;-------------------------------------------------------------------------------
; $05
.proc SoundLoadSQ1MacroVolume
	; Must update sq1
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateSq1

	; Mask old volume
	lda #$F0
	and SoundSq1Config
	sta SoundSq1Config

	; Get new volume
	lda (SoundFramePtr),y
	iny

	; Reduce by channel volume
	sec
	sbc SoundSq1Volume
	bcs :+
		lda #$00
	:
	and #$0F

	; Add new volume
	ora SoundSq1Config
	sta SoundSq1Config

	; Return
	jmp SoundInterpret::loop
.endproc

; $06
.proc SoundLoadSQ2MacroVolume
	; Must update sq2
	sty SoundUpdateSq2

	; Mask old volume
	lda #$F0
	and SoundSq2Config
	sta SoundSq2Config

	; Get new volume
	lda (SoundFramePtr),y
	iny

	; Reduce by channel volume
	sec
	sbc SoundSq2Volume
	bcs :+
		lda #$00
	:
	and #$0F

	; Add new volume
	ora SoundSq2Config
	sta SoundSq2Config

	; Return
	jmp SoundInterpret::loop
.endproc

; $07
.proc SoundLoadTriangleMacroVolume
	; Triangle doesn't have volume
	jmp SoundInterpret::loop
.endproc

; $08
.proc SoundLoadNoiseMacroVolume
	; Must update noise
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty SoundUpdateNoise

	; Mask old volume
	lda #$F0
	and SoundNoiseConfig
	sta SoundNoiseConfig

	; Get new volume
	lda (SoundFramePtr),y
	iny

	; Store new channel volume
	sta SoundNoiseVolume
	
	; Add new volume
	ora SoundNoiseConfig
	sta SoundNoiseConfig

	; Return
	jmp SoundInterpret::loop
.endproc

;-------------------------------------------------------------------------------
;
; Reset macro
;
;-------------------------------------------------------------------------------

.macro SoundResetMacroX ptr, baseptr
	; Get base pointer and store in the macro pointer.
	; This effectively resets the macro and starts it from the beginning.
	lda baseptr
	sta ptr
	lda baseptr + 1
	sta ptr + 1
	jmp SoundInterpret::loop
.endmacro

; $20
.proc SoundResetMacro0
	SoundResetMacroX SoundMacro0Ptr, SoundMacro0PtrBase
.endproc
; $21
.proc SoundResetMacro1
	SoundResetMacroX SoundMacro1Ptr, SoundMacro1PtrBase
.endproc
; $22
.proc SoundResetMacro2
	SoundResetMacroX SoundMacro2Ptr, SoundMacro2PtrBase
.endproc
; $23
.proc SoundResetMacro3
	SoundResetMacroX SoundMacro3Ptr, SoundMacro3PtrBase
.endproc
; $24
.proc SoundResetMacro4
	SoundResetMacroX SoundMacro4Ptr, SoundMacro4PtrBase
.endproc
; $25
.proc SoundResetMacro5
	SoundResetMacroX SoundMacro5Ptr, SoundMacro5PtrBase
.endproc
; $26
.proc SoundResetMacro6
	SoundResetMacroX SoundMacro6Ptr, SoundMacro6PtrBase
.endproc
; $27
.proc SoundResetMacro7
	SoundResetMacroX SoundMacro7Ptr, SoundMacro7PtrBase
.endproc

;-------------------------------------------------------------------------------
;
; Slide up
;
;-------------------------------------------------------------------------------

.macro SoundSlideup channelUpdate, channelFreq
	; Must update channel
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty channelUpdate

	; Get slide amount
	lda (SoundFramePtr),y
	iny

	; Add to frequency
	clc
	adc channelFreq
	sta channelFreq
	bcc :+
		inc channelFreq + 1
	:

	; Return
	jmp SoundInterpret::loop
.endmacro

.proc SoundSQ1SlideUp
	SoundSlideup SoundUpdateSq1, SoundSq1Freq
.endproc

.proc SoundSQ2SlideUp
	SoundSlideup SoundUpdateSq2, SoundSq2Freq
.endproc

.proc SoundTriangleSlideUp
	SoundSlideup SoundUpdateTriangle, SoundTriFreq
.endproc

.proc SoundNoiseSlideUp
	SoundSlideup SoundUpdateNoise, SoundNoiseFreq
.endproc

;-------------------------------------------------------------------------------
;
; Slide down
;
;-------------------------------------------------------------------------------

.macro SoundSlidedown channelUpdate, channelFreq
	; Must update channel
	; Y should always be greater than 0 at this point, and it saves
	; us some cycles now having to LDA anything.
	sty channelUpdate

	; Get slide amount
	lda (SoundFramePtr),y
	iny

	; Subtract from frequency
	eor #$ff
	sec
	adc channelFreq
	sta channelFreq
	bcs :+
		dec channelFreq + 1
	:

	; Return
	jmp SoundInterpret::loop
.endmacro

.proc SoundSQ1SlideDown
	SoundSlidedown SoundUpdateSq1, SoundSq1Freq
.endproc

.proc SoundSQ2SlideDown
	SoundSlidedown SoundUpdateSq2, SoundSq2Freq
.endproc

.proc SoundTriangleSlideDown
	SoundSlidedown SoundUpdateTriangle, SoundTriFreq
.endproc

.proc SoundNoiseSlideDown
	SoundSlidedown SoundUpdateNoise, SoundNoiseFreq
.endproc


;-------------------------------------------------------------------------------
;
; Jump table
;
;-------------------------------------------------------------------------------

SoundJumpTableLo:
	; Control
	.byte <(SoundInterpret::end-1)      ; 00 - End interpretation

	; Period
	.byte <(SoundLoadSQ1Period-1)       ; 01 - Square 1 period
	.byte <(SoundLoadSQ2Period-1)       ; 02 - Square 2 period
	.byte <(SoundLoadTrianglePeriod-1)  ; 03 - Triangle period
	.byte <(SoundLoadNoisePeriod-1)     ; 04 - Noise period

	; Volume
	.byte <(SoundLoadSQ1Volume-1)       ; 05 - Square 1 volume
	.byte <(SoundLoadSQ2Volume-1)       ; 06 - Square 2 volume
	.byte <(SoundLoadTriangleVolume-1)  ; 07 - Triangle volume
	.byte <(SoundLoadNoiseVolume-1)     ; 08 - Noise volume

	; Config
	.byte <(SoundLoadSQ1Config-1)       ; 09 - Square 1 config
	.byte <(SoundLoadSQ2Config-1)       ; 0A - Square 2 config
	.byte <(SoundLoadTriangleConfig-1)  ; 0B - Triangle config
	.byte <(SoundLoadNoiseConfig-1)     ; 0C - Noise config

	; Control 2
	.byte <(SoundInterpret::end-1)      ; 0D - Unused
	.byte <(SoundResetTimer-1)          ; 0E - Reset timer
	.byte <(SoundGoto-1)                ; 0F - Goto (Writes to frame pointer)

	; Macro control
	.byte <(SoundSetMacro0-1)           ; 10 - Set Macro 0 pointer & enable
	.byte <(SoundSetMacro1-1)           ; 11 - Set Macro 1 pointer & enable
	.byte <(SoundSetMacro2-1)           ; 12 - Set Macro 2 pointer & enable
	.byte <(SoundSetMacro3-1)           ; 13 - Set Macro 3 pointer & enable
	.byte <(SoundSetMacro4-1)           ; 14 - Set Macro 4 pointer & enable
	.byte <(SoundSetMacro5-1)           ; 15 - Set Macro 5 pointer & enable
	.byte <(SoundSetMacro6-1)           ; 16 - Set Macro 6 pointer & enable
	.byte <(SoundSetMacro7-1)           ; 17 - Set Macro 7 pointer & enable

	; Macro volume
	.byte <(SoundLoadSQ1MacroVolume-1)      ; 18 - Sq1 Macro Volume
	.byte <(SoundLoadSQ2MacroVolume-1)      ; 19 - Sq2 Macro Volume
	.byte <(SoundLoadTriangleMacroVolume-1) ; 1A - Triangle Macro Volume
	.byte <(SoundLoadNoiseMacroVolume-1)    ; 1B - Noise Macro Volume

	; Unused
	.byte <(SoundInterpret::end-1)      ; 1C - Unused
	.byte <(SoundInterpret::end-1)      ; 1D - Unused
	.byte <(SoundInterpret::end-1)      ; 1E - Unused
	.byte <(SoundInterpret::end-1)      ; 1F - Unused

	; Macro control
	.byte <(SoundResetMacro0-1)         ; 20 - Reset Macro 0 pointer
	.byte <(SoundResetMacro1-1)         ; 21 - Reset Macro 1 pointer
	.byte <(SoundResetMacro2-1)         ; 22 - Reset Macro 2 pointer
	.byte <(SoundResetMacro3-1)         ; 23 - Reset Macro 3 pointer
	.byte <(SoundResetMacro4-1)         ; 24 - Reset Macro 4 pointer
	.byte <(SoundResetMacro5-1)         ; 25 - Reset Macro 5 pointer
	.byte <(SoundResetMacro6-1)         ; 26 - Reset Macro 6 pointer
	.byte <(SoundResetMacro7-1)         ; 27 - Reset Macro 7 pointer

	; Unused
	.byte <(SoundInterpret::end-1)      ; 28 - Unused
	.byte <(SoundInterpret::end-1)      ; 29 - Unused
	.byte <(SoundInterpret::end-1)      ; 2A - Unused
	.byte <(SoundInterpret::end-1)      ; 2B - Unused
	.byte <(SoundInterpret::end-1)      ; 2C - Unused
	.byte <(SoundInterpret::end-1)      ; 2D - Unused
	.byte <(SoundInterpret::end-1)      ; 2E - Unused
	.byte <(SoundInterpret::end-1)      ; 2F - Unused

	; Sliding up
	.byte <(SoundSQ1SlideUp-1)          ; 30 - Square 1 slide up
	.byte <(SoundSQ2SlideUp-1)          ; 31 - Square 2 slide up
	.byte <(SoundTriangleSlideUp-1)     ; 32 - Triangle slide up
	.byte <(SoundNoiseSlideUp-1)        ; 33 - Noise slide up

	; Sliding down
	.byte <(SoundSQ1SlideDown-1)        ; 34 - Square 1 slide down
	.byte <(SoundSQ2SlideDown-1)        ; 35 - Square 2 slide down
	.byte <(SoundTriangleSlideDown-1)   ; 36 - Triangle slide down
	.byte <(SoundNoiseSlideDown-1)      ; 37 - Noise slide down

SoundJumpTableHi:
	;Control
	.byte >(SoundInterpret::end-1)      ; 00 - End interpretation

	;Period
	.byte >(SoundLoadSQ1Period-1)       ; 01 - Square 1 period
	.byte >(SoundLoadSQ2Period-1)       ; 02 - Square 2 period
	.byte >(SoundLoadTrianglePeriod-1)  ; 03 - Triangle period
	.byte >(SoundLoadNoisePeriod-1)     ; 04 - Noise period

	; Volume
	.byte >(SoundLoadSQ1Volume-1)       ; 05 - Square 1 volume
	.byte >(SoundLoadSQ2Volume-1)       ; 06 - Square 2 volume
	.byte >(SoundLoadTriangleVolume-1)  ; 07 - Triangle volume
	.byte >(SoundLoadNoiseVolume-1)     ; 08 - Noise volume

	; Config
	.byte >(SoundLoadSQ1Config-1)       ; 09 - Square 1 config
	.byte >(SoundLoadSQ2Config-1)       ; 0A - Square 2 config
	.byte >(SoundLoadTriangleConfig-1)  ; 0B - Triangle config
	.byte >(SoundLoadNoiseConfig-1)     ; 0C - Noise config

	; Control 2
	.byte >(SoundInterpret::end-1)      ; 0D - Unused
	.byte >(SoundResetTimer-1)          ; 0E - Reset timer
	.byte >(SoundGoto-1)                ; 0F - Goto (Writes to frame pointer)

	; Macro control
	.byte >(SoundSetMacro0-1)           ; 10 - Set Macro 0 pointer & enable
	.byte >(SoundSetMacro1-1)           ; 11 - Set Macro 1 pointer & enable
	.byte >(SoundSetMacro2-1)           ; 12 - Set Macro 2 pointer & enable
	.byte >(SoundSetMacro3-1)           ; 13 - Set Macro 3 pointer & enable
	.byte >(SoundSetMacro4-1)           ; 14 - Set Macro 4 pointer & enable
	.byte >(SoundSetMacro5-1)           ; 15 - Set Macro 5 pointer & enable
	.byte >(SoundSetMacro6-1)           ; 16 - Set Macro 6 pointer & enable
	.byte >(SoundSetMacro7-1)           ; 17 - Set Macro 7 pointer & enable

	; Macro volume
	.byte >(SoundLoadSQ1MacroVolume-1)      ; 18 - Sq1 Macro Volume
	.byte >(SoundLoadSQ2MacroVolume-1)      ; 19 - Sq2 Macro Volume
	.byte >(SoundLoadTriangleMacroVolume-1) ; 1A - Triangle Macro Volume
	.byte >(SoundLoadNoiseMacroVolume-1)    ; 1B - Noise Macro Volume

	; Unused
	.byte >(SoundInterpret::end-1)      ; 1C - Unused
	.byte >(SoundInterpret::end-1)      ; 1D - Unused
	.byte >(SoundInterpret::end-1)      ; 1E - Unused
	.byte >(SoundInterpret::end-1)      ; 1F - Unused

	; Macro control
	.byte >(SoundResetMacro0-1)         ; 20 - Reset Macro 0 pointer
	.byte >(SoundResetMacro1-1)         ; 21 - Reset Macro 1 pointer
	.byte >(SoundResetMacro2-1)         ; 22 - Reset Macro 2 pointer
	.byte >(SoundResetMacro3-1)         ; 23 - Reset Macro 3 pointer
	.byte >(SoundResetMacro4-1)         ; 24 - Reset Macro 4 pointer
	.byte >(SoundResetMacro5-1)         ; 25 - Reset Macro 5 pointer
	.byte >(SoundResetMacro6-1)         ; 26 - Reset Macro 6 pointer
	.byte >(SoundResetMacro7-1)         ; 27 - Reset Macro 7 pointer

	; Unused
	.byte >(SoundInterpret::end-1)      ; 28 - Unused
	.byte >(SoundInterpret::end-1)      ; 29 - Unused
	.byte >(SoundInterpret::end-1)      ; 2A - Unused
	.byte >(SoundInterpret::end-1)      ; 2B - Unused
	.byte >(SoundInterpret::end-1)      ; 2C - Unused
	.byte >(SoundInterpret::end-1)      ; 2D - Unused
	.byte >(SoundInterpret::end-1)      ; 2E - Unused
	.byte >(SoundInterpret::end-1)      ; 2F - Unused

	; Sliding up
	.byte >(SoundSQ1SlideUp-1)          ; 30 - Square 1 slide up
	.byte >(SoundSQ2SlideUp-1)          ; 31 - Square 2 slide up
	.byte >(SoundTriangleSlideUp-1)     ; 32 - Triangle slide up
	.byte >(SoundNoiseSlideUp-1)        ; 33 - Noise slide up

	; Sliding down
	.byte >(SoundSQ1SlideDown-1)        ; 34 - Square 1 slide down
	.byte >(SoundSQ2SlideDown-1)        ; 35 - Square 2 slide down
	.byte >(SoundTriangleSlideDown-1)   ; 36 - Triangle slide down
	.byte >(SoundNoiseSlideDown-1)      ; 37 - Noise slide down

;-------------------------------------------------------------------------------
;
; Exports
;
;-------------------------------------------------------------------------------

.export SoundInit
.export SoundUpdate
.export SoundPlay
.export SoundStop
.export SoundLoadTrack
