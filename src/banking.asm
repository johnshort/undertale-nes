;-------------------------------------------------------------------------------
;
; Undertale NES Demo
; Banking routines
;
; Original game by Toby Fox
; This port is written by Cytlan
;
; Copyright 2018 Cytlan
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
; 
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
; 
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
; 
; THIS  SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND  CONTRIBUTORS  "AS IS"
; AND  ANY  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT  LIMITED  TO,  THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  LIABLE
; FOR  ANY  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,  OR  CONSEQUENTIAL
; DAMAGES  (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS  OR
; SERVICES;  LOSS  OF USE, DATA, OR PROFITS; OR BUSINESS  INTERRUPTION)  HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  USE
; OF  THIS  SOFTWARE,  EVEN  IF  ADVISED OF  THE  POSSIBILITY  OF  SUCH  DAMAGE.
;
; Created:   2018-10-25
; Modified:  2018-10-25
;-------------------------------------------------------------------------------

.include "options.inc"
.include "macros.inc"

.autoimport +
.importzp MMC3LowerBank
.importzp BankTrampoline

.segment "Banking"

TrampolineCode:
	jsr $0000
	rts
TrampolineCodeEnd:

.proc BankingInitialize
	; Copy trampoline code
	ldx #0
	:
		lda TrampolineCode,x
		sta BankTrampoline,x
		inx
		cpx #TrampolineCodeEnd-TrampolineCode
	bne :-

	; Clear current selected bank
	lda #0
	sta MMC3LowerBank

	rts
.endproc

;
; Jump to a subroutine in a different bank
; This routine is very slow compared to a regular JSR, but it's
; completely state-agnostic, which is nice.
;
.proc JumpToBankSubroutine
	; Set up JSR in trampoline
	sty BankTrampoline+1
	stx BankTrampoline+2

	; Save old bank
	ldx MMC3LowerBank

	; Store new bank
	ldy #$06
	sty MMC3_BANK_SELECT
	sta MMC3_BANK_DATA
	sta MMC3LowerBank

	; Push old bank to stack, so that we can retrive it when we return
	txa
	pha

	; Jump to trampoline
	jsr BankTrampoline

	; Restore old bank
	lda #$06
	sta MMC3_BANK_SELECT
	pla
	sta MMC3_BANK_DATA
	sta MMC3LowerBank	

	rts
.endproc

.export BankingInitialize
.export JumpToBankSubroutine
