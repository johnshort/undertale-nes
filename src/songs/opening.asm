.include "options.inc"
.include "macros.inc"

;-------------------------------------------------------------------------------
;
; Opening Macros
;
;-------------------------------------------------------------------------------
Opening_Macro_Volume_0_SQ1:
	S_SQ1_MACRO_VOLUME 1
	S_END
	S_SQ1_MACRO_VOLUME 14
	S_END
	S_SQ1_MACRO_VOLUME 13
	S_END
	S_SQ1_MACRO_VOLUME 13
	S_END
	S_SQ1_MACRO_VOLUME 13
	S_END
	S_SQ1_MACRO_VOLUME 12
	S_END
	S_SQ1_MACRO_VOLUME 12
	S_END
	S_SQ1_MACRO_VOLUME 12
	S_END
	S_SQ1_MACRO_VOLUME 11
	S_END
	S_SQ1_MACRO_VOLUME 11
	S_END
	S_SQ1_MACRO_VOLUME 10
	S_END
	S_SQ1_MACRO_VOLUME 10
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 0
Opening_Macro_Volume_0_SQ1_Loop:
	S_GOTO Opening_Macro_Volume_0_SQ1_Loop

Opening_Macro_Volume_1_SQ1:
	S_SQ1_MACRO_VOLUME 2
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 11
	S_END
	S_SQ1_MACRO_VOLUME 14
	S_END
	S_SQ1_MACRO_VOLUME 15
	S_END
	S_SQ1_MACRO_VOLUME 14
	S_END
	S_SQ1_MACRO_VOLUME 14
	S_END
	S_SQ1_MACRO_VOLUME 13
	S_END
	S_SQ1_MACRO_VOLUME 13
	S_END
	S_SQ1_MACRO_VOLUME 12
	S_END
	S_SQ1_MACRO_VOLUME 12
	S_END
	S_SQ1_MACRO_VOLUME 12
	S_END
	S_SQ1_MACRO_VOLUME 11
	S_END
	S_SQ1_MACRO_VOLUME 11
	S_END
	S_SQ1_MACRO_VOLUME 10
	S_END
	S_SQ1_MACRO_VOLUME 10
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 9
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 8
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 7
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 6
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 5
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 4
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
	S_END
	S_SQ1_MACRO_VOLUME 3
Opening_Macro_Volume_1_SQ1_Loop:
	S_GOTO Opening_Macro_Volume_1_SQ1_Loop

Opening_Macro_Volume_1_SQ2:
	S_SQ2_MACRO_VOLUME 2
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 11
	S_END
	S_SQ2_MACRO_VOLUME 14
	S_END
	S_SQ2_MACRO_VOLUME 15
	S_END
	S_SQ2_MACRO_VOLUME 14
	S_END
	S_SQ2_MACRO_VOLUME 14
	S_END
	S_SQ2_MACRO_VOLUME 13
	S_END
	S_SQ2_MACRO_VOLUME 13
	S_END
	S_SQ2_MACRO_VOLUME 12
	S_END
	S_SQ2_MACRO_VOLUME 12
	S_END
	S_SQ2_MACRO_VOLUME 12
	S_END
	S_SQ2_MACRO_VOLUME 11
	S_END
	S_SQ2_MACRO_VOLUME 11
	S_END
	S_SQ2_MACRO_VOLUME 10
	S_END
	S_SQ2_MACRO_VOLUME 10
	S_END
	S_SQ2_MACRO_VOLUME 9
	S_END
	S_SQ2_MACRO_VOLUME 9
	S_END
	S_SQ2_MACRO_VOLUME 9
	S_END
	S_SQ2_MACRO_VOLUME 9
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 7
	S_END
	S_SQ2_MACRO_VOLUME 7
	S_END
	S_SQ2_MACRO_VOLUME 7
	S_END
	S_SQ2_MACRO_VOLUME 7
	S_END
	S_SQ2_MACRO_VOLUME 7
	S_END
	S_SQ2_MACRO_VOLUME 7
	S_END
	S_SQ2_MACRO_VOLUME 6
	S_END
	S_SQ2_MACRO_VOLUME 6
	S_END
	S_SQ2_MACRO_VOLUME 6
	S_END
	S_SQ2_MACRO_VOLUME 6
	S_END
	S_SQ2_MACRO_VOLUME 6
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 4
	S_END
	S_SQ2_MACRO_VOLUME 4
	S_END
	S_SQ2_MACRO_VOLUME 4
	S_END
	S_SQ2_MACRO_VOLUME 4
	S_END
	S_SQ2_MACRO_VOLUME 4
	S_END
	S_SQ2_MACRO_VOLUME 3
	S_END
	S_SQ2_MACRO_VOLUME 3
	S_END
	S_SQ2_MACRO_VOLUME 3
	S_END
	S_SQ2_MACRO_VOLUME 3
	S_END
	S_SQ2_MACRO_VOLUME 3
Opening_Macro_Volume_1_SQ2_Loop:
	S_GOTO Opening_Macro_Volume_1_SQ2_Loop

Opening_Macro_Volume_2_SQ2:
	S_SQ2_MACRO_VOLUME 1
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 15
	S_END
	S_SQ2_MACRO_VOLUME 12
	S_END
	S_SQ2_MACRO_VOLUME 10
	S_END
	S_SQ2_MACRO_VOLUME 8
	S_END
	S_SQ2_MACRO_VOLUME 5
	S_END
	S_SQ2_MACRO_VOLUME 3
	S_END
	S_SQ2_MACRO_VOLUME 1
	S_END
	S_SQ2_MACRO_VOLUME 1
	S_END
	S_SQ2_MACRO_VOLUME 1
	S_END
	S_SQ2_MACRO_VOLUME 0
	S_END
	S_SQ2_MACRO_VOLUME 0
	S_END
	S_SQ2_MACRO_VOLUME 0
Opening_Macro_Volume_2_SQ2_Loop:
	S_GOTO Opening_Macro_Volume_2_SQ2_Loop

Opening_Macro_Volume_3_NOISE:
	S_NOISE_MACRO_VOLUME 15
	S_END
	S_NOISE_MACRO_VOLUME 8
	S_END
	S_NOISE_MACRO_VOLUME 1
	S_END
	S_NOISE_MACRO_VOLUME 0
Opening_Macro_Volume_3_NOISE_Loop:
	S_GOTO Opening_Macro_Volume_3_NOISE_Loop

Opening_Macro_Volume_4_NOISE:
	S_NOISE_MACRO_VOLUME 15
	S_END
	S_NOISE_MACRO_VOLUME 13
	S_END
	S_NOISE_MACRO_VOLUME 9
	S_END
	S_NOISE_MACRO_VOLUME 7
	S_END
	S_NOISE_MACRO_VOLUME 5
	S_END
	S_NOISE_MACRO_VOLUME 3
	S_END
	S_NOISE_MACRO_VOLUME 2
	S_END
	S_NOISE_MACRO_VOLUME 1
	S_END
	S_NOISE_MACRO_VOLUME 0
	S_END
	S_NOISE_MACRO_VOLUME 0
	S_END
	S_NOISE_MACRO_VOLUME 0
Opening_Macro_Volume_4_NOISE_Loop:
	S_GOTO Opening_Macro_Volume_4_NOISE_Loop

Opening_Macro_Pitch_0_TRIANGLE:
Opening_Macro_Pitch_0_TRIANGLE_Loop:
	S_END
	S_TRIANGLE_SLIDEUP 1
	S_END
	S_END
	S_TRIANGLE_SLIDEUP 1
	S_END
	S_END
	S_TRIANGLE_SLIDEUP 1
	S_END
	S_END
	S_TRIANGLE_SLIDEDOWN 1
	S_END
	S_END
	S_TRIANGLE_SLIDEDOWN 1
	S_END
	S_END
	S_TRIANGLE_SLIDEDOWN 1
	S_GOTO Opening_Macro_Pitch_0_TRIANGLE_Loop

;-------------------------------------------------------------------------------
;
; Track: Opening
;
;-------------------------------------------------------------------------------
Opening_Frame_0:
	S_TIME 0
	S_SQ1_NOTE S_C5
	S_SET_MACRO 0, Opening_Macro_Volume_0_SQ1
	S_SQ1_DUTY 2
	S_SQ2_CONFIG $30
	S_TRIANGLE_NOTE S_F4
	S_SET_MACRO 1, Opening_Macro_Pitch_0_TRIANGLE
	S_TRIANGLE_CONFIG $30
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_Gs4
	S_RESET_MACRO 1
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G4
	S_RESET_MACRO 1
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_1

Opening_Frame_1:
	S_TIME 0
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_F4
	S_RESET_MACRO 1
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_D6
	S_RESET_MACRO 0
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_Gs4
	S_RESET_MACRO 1
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G4
	S_RESET_MACRO 1
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_2

Opening_Frame_2:
	S_TIME 0
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_SQ2_CONFIG $30
	S_TRIANGLE_NOTE S_F4
	S_RESET_MACRO 1
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_Gs4
	S_RESET_MACRO 1
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G4
	S_RESET_MACRO 1
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_3

Opening_Frame_3:
	S_TIME 0
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_F4
	S_RESET_MACRO 1
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_D6
	S_RESET_MACRO 0
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_Gs4
	S_RESET_MACRO 1
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G4
	S_RESET_MACRO 1
	S_END

	S_TIME 6461538
	S_SQ1_CONFIG $30
	S_END

	S_TIME 7153846
	S_SQ1_NOTE S_As3
	S_SET_MACRO 0, Opening_Macro_Volume_1_SQ1
	S_SQ1_DUTY 1
	S_END

	S_TIME 7269231
	S_SQ1_NOTE S_B3
	S_RESET_MACRO 0
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_4

Opening_Frame_4:
	S_TIME 0
	S_SQ1_NOTE S_C4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_As4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_NOISE_CONFIG $30
	S_END

	S_TIME 461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_SET_MACRO 3, Opening_Macro_Volume_2_SQ2
	S_SQ2_DUTY 2
	S_END

	S_TIME 692308
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 1153846
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1384615
	S_SQ1_NOTE S_A4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 1615385
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_As4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 2307692
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 2538462
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_E4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 3000000
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3230769
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 3461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 4153846
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 4384615
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_E5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 4846154
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5076923
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 5307692
	S_SQ1_NOTE S_Ds5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5423077
	S_SQ1_NOTE S_D5
	S_RESET_MACRO 0
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_Gs4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 6000000
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 6230769
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6461538
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 6692308
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6923077
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 7153846
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_5

Opening_Frame_5:
	S_TIME 0
	S_SQ1_NOTE S_D4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 692308
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 1153846
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1384615
	S_SQ1_NOTE S_A4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 1615385
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_C5
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 2307692
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 2538462
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 3000000
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3230769
	S_SQ1_NOTE S_A4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 3461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_E4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 4153846
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 4384615
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 4846154
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5076923
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 5307692
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 6000000
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_TRIANGLE_NOTE S_F4
	S_RESET_MACRO 1
	S_END

	S_TIME 6230769
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6461538
	S_SQ1_NOTE S_E4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_E3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_TRIANGLE_NOTE S_E4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 6692308
	S_SQ2_NOTE S_E3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6923077
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_C3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_TRIANGLE_NOTE S_C4
	S_RESET_MACRO 1
	S_END

	S_TIME 7153846
	S_SQ2_NOTE S_C3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_6

Opening_Frame_6:
	S_TIME 0
	S_SQ1_NOTE S_C4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_As4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 692308
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 1153846
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1384615
	S_SQ1_NOTE S_A4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 1615385
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_As4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 2307692
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 2538462
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_E4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 3000000
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3230769
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 3461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 4153846
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 4384615
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_E5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 4846154
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5076923
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 5307692
	S_SQ1_NOTE S_Ds5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5423077
	S_SQ1_NOTE S_D5
	S_RESET_MACRO 0
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_Gs4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 6000000
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 6230769
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6461538
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 6692308
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6923077
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 7153846
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_7

Opening_Frame_7:
	S_TIME 0
	S_SQ1_NOTE S_D4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 692308
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 1153846
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1384615
	S_SQ1_NOTE S_A4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 1615385
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_C5
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 2307692
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 2538462
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 3000000
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3230769
	S_SQ1_NOTE S_A4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 3461538
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_E4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_A4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 4153846
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 4384615
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 4846154
	S_SQ2_NOTE S_G3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5076923
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_END

	S_TIME 5307692
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_G4
	S_RESET_MACRO 0
	S_NOISE_NOTE S_N1
	S_SET_MACRO 2, Opening_Macro_Volume_4_NOISE
	S_END

	S_TIME 6000000
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_TRIANGLE_NOTE S_F4
	S_RESET_MACRO 1
	S_END

	S_TIME 6230769
	S_SQ2_NOTE S_F3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6461538
	S_SQ1_NOTE S_E4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_E3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_TRIANGLE_NOTE S_E4
	S_RESET_MACRO 1
	S_NOISE_NOTE S_NC
	S_SET_MACRO 2, Opening_Macro_Volume_3_NOISE
	S_END

	S_TIME 6692308
	S_SQ2_NOTE S_E3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 6923077
	S_SQ1_NOTE S_F4
	S_RESET_MACRO 0
	S_SQ2_NOTE S_C3
	S_SQ2_VOLUME 8
	S_RESET_MACRO 3
	S_TRIANGLE_NOTE S_C4
	S_RESET_MACRO 1
	S_END

	S_TIME 7153846
	S_SQ2_NOTE S_C3
	S_SQ2_VOLUME 4
	S_RESET_MACRO 3
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_8

Opening_Frame_8:
	S_TIME 0
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_SQ2_VOLUME 4
	S_TRIANGLE_NOTE S_F5
	S_RESET_MACRO 1
	S_END

	S_TIME 230769
	S_SQ2_NOTE S_C6
	S_SET_MACRO 3, Opening_Macro_Volume_1_SQ2
	S_SQ2_DUTY 1
	S_END

	S_TIME 461538
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 692308
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 1153846
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 1384615
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 1615385
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_B4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G5
	S_RESET_MACRO 1
	S_END

	S_TIME 2076923
	S_SQ2_NOTE S_B4
	S_RESET_MACRO 3
	S_END

	S_TIME 2307692
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 2538462
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 3000000
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 3230769
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 3461538
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_As4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_As4
	S_RESET_MACRO 1
	S_END

	S_TIME 3923077
	S_SQ2_NOTE S_As4
	S_RESET_MACRO 3
	S_END

	S_TIME 4153846
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 4384615
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 4846154
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 5076923
	S_SQ1_NOTE S_As5
	S_RESET_MACRO 0
	S_END

	S_TIME 5307692
	S_SQ2_NOTE S_As5
	S_RESET_MACRO 3
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_A5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_F5
	S_RESET_MACRO 1
	S_END

	S_TIME 5769231
	S_SQ2_NOTE S_A5
	S_RESET_MACRO 3
	S_END

	S_TIME 6000000
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 6230769
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 6461538
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 6692308
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 6923077
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_END

	S_TIME 7153846
	S_SQ2_NOTE S_C5
	S_RESET_MACRO 3
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_9

Opening_Frame_9:
	S_TIME 0
	S_SQ1_NOTE S_C6
	S_RESET_MACRO 0
	S_SQ2_VOLUME 4
	S_TRIANGLE_NOTE S_F5
	S_RESET_MACRO 1
	S_END

	S_TIME 230769
	S_SQ2_NOTE S_C6
	S_RESET_MACRO 3
	S_END

	S_TIME 461538
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 692308
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 923077
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 1153846
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 1384615
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 1615385
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 1846154
	S_SQ1_NOTE S_B4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_G5
	S_RESET_MACRO 1
	S_END

	S_TIME 2076923
	S_SQ2_NOTE S_B4
	S_RESET_MACRO 3
	S_END

	S_TIME 2307692
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 2538462
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 2769231
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 3000000
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 3230769
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 3461538
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 3692308
	S_SQ1_NOTE S_As4
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_As4
	S_RESET_MACRO 1
	S_END

	S_TIME 3923077
	S_SQ2_NOTE S_As4
	S_RESET_MACRO 3
	S_END

	S_TIME 4153846
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 4384615
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 4615385
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 4846154
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 5076923
	S_SQ1_NOTE S_As5
	S_RESET_MACRO 0
	S_END

	S_TIME 5307692
	S_SQ2_NOTE S_As5
	S_RESET_MACRO 3
	S_END

	S_TIME 5538462
	S_SQ1_NOTE S_A5
	S_RESET_MACRO 0
	S_TRIANGLE_NOTE S_F5
	S_RESET_MACRO 1
	S_END

	S_TIME 5769231
	S_SQ2_NOTE S_A5
	S_RESET_MACRO 3
	S_END

	S_TIME 6000000
	S_SQ1_NOTE S_G5
	S_RESET_MACRO 0
	S_END

	S_TIME 6230769
	S_SQ2_NOTE S_G5
	S_RESET_MACRO 3
	S_END

	S_TIME 6461538
	S_SQ1_NOTE S_F5
	S_RESET_MACRO 0
	S_END

	S_TIME 6692308
	S_SQ2_NOTE S_F5
	S_RESET_MACRO 3
	S_END

	S_TIME 6923077
	S_SQ1_NOTE S_C5
	S_RESET_MACRO 0
	S_END

	S_TIME 7153846
	S_SQ2_NOTE S_C5
	S_RESET_MACRO 3
	S_END

	S_TIME 7269231
	S_END

	S_TIME 7384615
	S_RESET_TIMER
	S_GOTO Opening_Frame_0

.export Opening_Frame_0
