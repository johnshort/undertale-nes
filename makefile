# ------------------------------------------------------------------------------
#
# Makefile for Undertale NES
#
# POSIX shell tools needed!
#
# ------------------------------------------------------------------------------

# Default target CPU of the assembly sources
CPU         := 6502

# Name of the target ROM
TARGET      := undertale.nes

# Config file containing the memory map, etc.
CONFIG      := layout.conf

# Directories
SRCDIR      := src
INCDIR      := inc res
BUILDDIR    := obj
TARGETDIR   := bin
TOOLSDIR    := tools

# C sources
CSOURCES    :=

# Assembly sources
ASMSOURCES  := \
	src/songs/opening.asm \
	src/banking.asm \
	src/chr.asm \
	src/header.asm \
	src/init.asm \
	src/reset.asm \
	src/sound.asm \
	src/vectors.asm \
	src/zeropage.asm

# Additional libraries
LIBRARIES   := 

# ------------------------------------------------------------------------------
#
# Tools
#
# ------------------------------------------------------------------------------

# Compile & Link, Compile, Assemble and Link utilities
CL65 = tools/cc65/bin/cl65
CC65 = tools/cc65/bin/cc65
CA65 = tools/cc65/bin/ca65
LD65 = tools/cc65/bin/ld65

dbgfilefix = $(TOOLSDIR)/dbgfilefix/bin/dbgfilefix

# Compiler, assembler and linker options
CFLAGS = -g
AFLAGS = -g
LDFLAGS = --dbgfile $(TARGETDIR)/undertale.nes.dbg

# System utilities 
# RM - Some utility that can delete files
RM = rm -rf

# ------------------------------------------------------------------------------
#
# Messages
#
# ------------------------------------------------------------------------------

M_CLEANING   = Cleaning

# ------------------------------------------------------------------------------
#
# Flags and argument finalization
#
# ------------------------------------------------------------------------------

# Finalizing flags
CFLAGS += $(patsubst %,-I%,$(INCDIR))
AFLAGS += $(patsubst %,-I%,$(INCDIR))

# Get object names
OBJS_ = $(CSOURCES:.c=.o) $(ASMSOURCES:.asm=.o)
OBJS = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(OBJS_)) $(LIBRARIES)
PATHS = $(sort $(dir $(OBJS)))

# ------------------------------------------------------------------------------
#
# Targets
#
# ------------------------------------------------------------------------------

all: clean_all makedirs $(TARGETDIR)/$(TARGET)

makedirs:
	echo $(OBJS)
	mkdir -p $(PATHS)

$(TARGETDIR)/$(TARGET): $(OBJS) $(LIBRARIES)
	$(LD65) $(LDFLAGS) --config $(CONFIG) --obj $(OBJS) -o $(TARGETDIR)/$(TARGET)
	$(dbgfilefix) $(TARGETDIR)/$(TARGET).dbg

$(BUILDDIR)/%.o: %.c
	$(CC65) $(CFLAGS) -O -o $@ $<

$(BUILDDIR)/%.o: $(SRCDIR)/%.asm
	$(CA65) $(AFLAGS) --cpu $(CPU) -o $@ $<

clean: start clean_all done

clean_all:
	@echo $(M_CLEANING)
	$(RM) $(TARGETDIR)/$(TARGET)
	$(RM) $(BUILDDIR)/*

.PHONY: all start done clean clean_all

# EOF --------------------------------------------------------------------------