//------------------------------------------------------------------------------
//
// Undertale NES Demo
// Famitracker Converter
//
// This converter takes the TXT format Famitracker exports, and turns it into
// a music assembly file for our sound engine.
// This converter is not very flexible or elegant, so don't expect it to survive
// any format changes or complicated songs!
// It's actually pretty dirty. Don't expect it to survive if you as much as
// sneeze on it.
// Oh, and don't use it for anything.
// It's a one trick pony.
// And I'm not proud of it.
//
// Copyright 2018 Cytlan
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Created:   2018-10-07
// Modified:  2018-10-14
//------------------------------------------------------------------------------

const fs = require('fs')

let file = fs.readFileSync('opening.txt', 'utf8').split('\n')

let song = {
	macros: [],
	instruments: [],
	tracks: []
}

let fields = {
	'TITLE': 'title',
	'AUTHOR': 'author',
	'COPYRIGHT': 'copyright',
	'COMMENT': 'comment',
	'MACHINE': 'machine',
	'FRAMERATE': 'framerate',
	'EXPANSION': 'expansion',
	'VIBRATO': 'vibrato',
	'SPLIT': 'split',
}

let track = null
let pattern = null

for(let i in file)
{
	let l = file[i]
	let t = l.split(' ')
	let token = t.shift()
	let args = t.join(' ').trim()
	if(token == '#')
		continue
	if(fields[token])
	{
		if(args[0] == '"')
			args = args.substring(1, args.length - 1)
		song[fields[token]] = args
		continue
	}
	switch(token)
	{
		case 'MACRO':
			let marg = args.split(/\s+/)
			let macro = {
				type: marg.shift(),
				index: marg.shift(),
				loopIndex: parseInt(marg.shift(), 10),
				releaseIndex: marg.shift(),
				ident: marg.shift()
			}
			if(marg.shift() != ':')
				throw new Error("Expected : in MACRO")
			macro.data = marg
			song.macros.push(macro)
			break
		case 'INST2A03':
			let iarg = args.split(/\s+/)
			let instrument = {
				index: parseInt(iarg.shift(), 10),
				volumeMacro: parseInt(iarg.shift(), 10),
				arpeggioMacro: parseInt(iarg.shift(), 10),
				pitchMacro: parseInt(iarg.shift(), 10),
				hiPitchMacro: parseInt(iarg.shift(), 10),
				dutyMacro: parseInt(iarg.shift(), 10),
				name: iarg.join(' ')
			}
			song.instruments.push(instrument)
			break
		case 'TRACK':
			let targ = args.trim().split(/\s+/)
			track = {
				rows: parseInt(targ[0], 10),
				speed: parseInt(targ[1], 10),
				tempo: parseInt(targ[2], 10),
				name: targ[3],
				order: [],
				patterns: []
			}
			track.bpm = (track.tempo * 6) / track.speed
			//track.ms = ((1000 * 60) / 4) / track.bpm
			track.frames = (60000 / (track.bpm * 4))/(1000/60)
			track.time = track.frames*(1000/60)*1000
			song.tracks.push(track)
			break
		case 'COLUMNS':
			track.columns = args
			break
		case 'ORDER':
			let oarg = args.trim().split(/\s+/)
			let order = []
			for(let i = 2; i < oarg.length; i++)
				order.push(parseInt(oarg[i], 16))
			track.order.push(order)
			break
		case 'PATTERN':
			pattern = {
				index: parseInt(args, 16),
				rows: []
			}
			track.patterns.push(pattern)
			break
		case 'ROW':
			let rarg = args.split(':')
			let row = {
				index: parseInt(rarg.shift().trim(), 16),
				columns: [],
			}
			row.time = (row.index * track.frames)*(1000/60)*1000
			for(let i in rarg)
			{
				let c = rarg[i].trim().split(/\s+/)
				let column = {}
				if(c[0] != '...')
					column.note = c[0]
				if(c[1] != '..')
					column.instrument = parseInt(c[1], 16)
				if(c[2] != '.')
					column.volume = parseInt(c[2], 16)
				if(c[3] != '...')
					column.effect = c[3]
				row.columns.push(column)
			}
			pattern.rows.push(row)
			break
	}
}

function makeNote(note, channel)
{
	if(note == '---')
		return 'S_0'
	//if(channel == 1)
		note = note.substring(0, 2)+(parseInt(note.substring(2, 3), 10)+1)
	if(channel == 2)
		note = 'N-'+note.substring(0, 1)
	return 'S_'+note[0]+(note[1] == '#' ? 's' : '')+note[2]
}

function sanitizeString(str)
{
	return str.replace(/[^A-Za-z0-9]/g, '')
}
song.sTitle = sanitizeString(song.title || 'untitled')

console.log(song.tracks[0])

let macros = {

}

for(let i in song.macros)
{
	let macro = song.macros[i]
	console.log(macro)
	let types = [
		'MACRO_VOLUME',
		'ARPEGGIO',
		'PITCH',
		'HIPITCH',
		'DUTY'
	]
	let typesName = [
		'Volume',
		'Arpeggio',
		'Pitch',
		'HiPitch',
		'Duty'
	]
	let baseLabel = song.sTitle+'_Macro_'+typesName[macro.type]+'_'+macro.index
	let macroObj = {
		labels: {},
		codes: {},
		implementations: {},
		used: {}
	}
	if(!macros[macro.type])
		macros[macro.type] = {}
	macros[macro.type][macro.index] = macroObj

	let channels = ['SQ1', 'SQ2', 'TRIANGLE', 'NOISE']
	for(let j in channels)
	{
		let c = channels[j]
		macroObj.codes[j] = []
		let label = baseLabel+'_'+c
		macroObj.labels[j] = label
		for(let k in macro.data)
		{
			let code = 'S_'+c+'_'+types[macro.type]+' '+macro.data[k]
			if(types[macro.type] == 'PITCH')
			{
				if(macro.data[k][0] == '-')
					code = 'S_'+c+'_SLIDEDOWN '+macro.data[k].substring(1)
				else
					code = 'S_'+c+'_SLIDEUP '+macro.data[k]
				if(macro.data[k] == '0')
					code = ''
			}
			macroObj.codes[j].push(code)
		}
		macroObj.implementations[j] =
			label+':\n'
		for(let k in macroObj.codes[j])
		{
			if(k == macro.loopIndex)
				macroObj.implementations[j] += label+'_Loop:\n'
			if(macroObj.codes[j][k])
				macroObj.implementations[j] += '\t'+macroObj.codes[j][k] + '\n'
			if(k == macroObj.codes[j].length - 1 && macro.loopIndex != -1)
				macroObj.implementations[j] += '\tS_GOTO '+label+'_Loop\n'
			else if(k == macroObj.codes[j].length - 1)
			{
				macroObj.implementations[j] += label+'_Loop:\n'
				macroObj.implementations[j] += '\tS_GOTO '+label+'_Loop\n'
			}
			else
				macroObj.implementations[j] += '\t'+'S_END\n'
		}
	}
	//console.log(macroObj.implementations[0])

}

let state = {
	SQ1_instrument: -1,
	SQ2_instrument: -1,
	TRIANGLE_instrument: -1,
	NOISE_instrument: -1,

	SQ1_macro: {},
	SQ2_macro: {},
	TRIANGLE_macro: {},
	NOISE_macro: {},

	channelActive: {
		SQ1: false,
		SQ2: false,
		TRIANGLE: false,
		NOISE: false,
	},

	macroSlots:
	[
		0, 0, 0, 0, 0, 0, 0, 0
	]
}

function getMacro()
{
	for(let i = 0; i < 8; i++)
	{
		if(!state.macroSlots[i])
		{
			state.macroSlots[i] = 1
			return i
		}
	}
	console.log(state)
	throw new Error("out of macros")
}

let songInsturctions = ''
let songTrack = song.tracks[0]
for(let ii in songTrack.order)
//if(1)
{
	//if(ii == 3) break
	console.log(ii)
	//let ii = 0
	let order = songTrack.order[ii]
	let code = song.sTitle+'_Frame_'+ii+':\n'
	for(let i = 0; i < songTrack.rows; i++)
	{
		let section = ''
		let channels = ['SQ1', 'SQ2', 'TRIANGLE', 'NOISE']

		let time = 0
		for(let j in channels)
		{
			let p = songTrack.patterns[order[j]]
			if(!p)
				continue
			let r = p.rows[i]
			let c = channels[j]
			if(!time && r.time)
				time = r.time
			if(r.columns[j].note == '---')
			{
				section += '\tS_'+c+'_CONFIG $30\n'
				state.channelActive[c] = false
			}
			else if(r.columns[j].note && c == 'TRIANGLE')
				section += '\tS_'+c+'_NOTE '+makeNote(r.columns[j].note, 1)+'\n'
			else if(r.columns[j].note && c == 'NOISE')
				section += '\tS_'+c+'_NOTE '+makeNote(r.columns[j].note, 2)+'\n'
			else if(r.columns[j].note)
				section += '\tS_'+c+'_NOTE '+makeNote(r.columns[j].note)+'\n'
			if(r.columns[j].volume)
				section += '\tS_'+c+'_VOLUME '+r.columns[j].volume+'\n'
			if(
				r.columns[j].instrument != undefined &&
				r.columns[j].instrument != state[c+'_instrument']
			)
			{
				state[c+'_instrument'] = r.columns[j].instrument
				let instrument = song.instruments[r.columns[j].instrument]
				if(instrument.volumeMacro != -1)
				{
					if(
						state[c+'_macro'][0] != undefined &&
						state[c+'_macro'][0] != -1
					)
						state.macroSlots[state[c+'_macro'][0]] = 0
					let mid = getMacro()
					section += 
						'\tS_SET_MACRO '+mid+', '+
						macros[0][instrument.volumeMacro].labels[j]+'\n'
					macros[0][instrument.volumeMacro].used[j] = true
					state[c+'_macro'][0] = mid
				}
				else
				{
					state[c+'_macro'][0] = -1
				}
				if(
					instrument.pitchMacro != -1 &&
					macros[2][instrument.pitchMacro])
				{
					if(
						state[c+'_macro'][2] != undefined &&
						state[c+'_macro'][2] != -1
					)
						state.macroSlots[state[c+'_macro'][2]] = 0
					let mid = getMacro()
					section += 
						'\tS_SET_MACRO '+mid+', '+
						macros[2][instrument.pitchMacro].labels[j]+'\n'
					macros[2][instrument.pitchMacro].used[j] = true
					state[c+'_macro'][2] = mid
				}
				else
				{
					state[c+'_macro'][2] = -1
				}
				if(instrument.dutyMacro != -1 && c != 'TRIANGLE' && c != 'NOISE')
				{
					// Usually duty macros are only 1 instruction long.
					// Just insert it into the stream if that's the case
					if(macros[4][instrument.dutyMacro].codes[j].length == 1)
					{
						section +=
							'\t'+macros[4][instrument.dutyMacro].codes[j][0]+
							'\n'
						state[c+'_macro'][4] = -1
						if(!state.channelActive[c])
							state.channelActive[c] = true
					}
					else
					{
						if(
							state[c+'_macro'][4] != undefined &&
							state[c+'_macro'][4] != -1
						)
							state.macroSlots[state[c+'_macro'][4]] = 0
						let mid = getMacro()
						section += 
							'\tS_SET_MACRO '+mid+', '+
							macros[4][instrument.dutyMacro].labels[j]+'\n'
						macros[4][instrument.dutyMacro].used[j] = true
						state[c+'_macro'][4] = mid
					}
				}
				else
				{
					state[c+'_macro'][4] = -1
				}
			}
			else if(
				r.columns[j].instrument != undefined &&
				r.columns[j].instrument == state[c+'_instrument']
			)
			{
				let instrument = song.instruments[r.columns[j].instrument]
				if(instrument.volumeMacro != -1 && state[c+'_macro'][0] != -1)
					section += '\tS_RESET_MACRO '+state[c+'_macro'][0]+'\n'

				if(instrument.pitchMacro != -1 && state[c+'_macro'][2] != -1)
					section += '\tS_RESET_MACRO '+state[c+'_macro'][2]+'\n'

				if(instrument.dutyMacro != -1 && state[c+'_macro'][4] != -1)
					section += '\tS_RESET_MACRO '+state[c+'_macro'][4]+'\n'
			}
			/*
			else if(
				r.columns[j].instrument == undefined && 
			)
			{
				state[c+'_macro'][0] = -1
				state[c+'_macro'][1] = -1
				state[c+'_macro'][2] = -1
				state[c+'_macro'][3] = -1
				state[c+'_macro'][4] = -1
			}
			*/
			if(
				!state.channelActive[c] && r.columns[j].note &&
				r.columns[j].note != '---'
			)
			{
				state.channelActive[c] = true
				section += '\tS_'+c+'_CONFIG $30\n'
			}
		}

		if(section || i == track.rows - 1)
		{
			code += '\tS_TIME '+Math.round(time)+'\n'
			code += section
			if(i == track.rows - 1)
			{
				code += '\tS_END\n\n'

				// If we don't do this, we'll be skipping the last row!
				// Not good...
				// I'm pretty drunk right now :^)
				code += '\tS_TIME '+Math.round(time+track.time)+'\n'
				code += '\tS_RESET_TIMER\n'
				let next = parseInt(ii, 10) + 1
				if(ii == songTrack.order.length - 1)
					next = 0
				code += '\tS_GOTO '+song.sTitle+'_Frame_'+next+'\n'
				code += '\n'
			}
			else
			{
				code += '\tS_END\n'
				code += '\n'
			}
		}
	}

	songInsturctions += code

	//console.log(macroCode)
	//console.log(songCode)

	//break
}

let headerCode =
	'.include "options.inc"\n'+
	'.include "macros.inc"\n\n'

// Find used macros
let macroCode = 
	';'+(new Array(80).join('-'))+'\n'+
	';\n'+
	'; '+song.title+' Macros\n'+
	';\n'+
	';'+(new Array(80).join('-'))+'\n'
for(let i in macros)
{
	let m = macros[i]
	for(let j in m)
	{
		let macro = m[j]
		let channels = ['SQ1', 'SQ2', 'TRIANGLE', 'NOISE']
		for(let k in channels)
		{
			if(macro.used[k])
			{
				macroCode += macro.implementations[k]+'\n'
			}
		}

	}
}

let songCode = 
	';'+(new Array(80).join('-'))+'\n'+
	';\n'+
	'; Track: '+song.title+'\n'+
	';\n'+
	';'+(new Array(80).join('-'))+'\n'+
	songInsturctions

let footerCode =
	'.export '+song.sTitle+'_Frame_0\n'

fs.writeFileSync(
	'../src/songs/'+song.sTitle.toLowerCase()+'.asm',
	headerCode+macroCode+songCode+footerCode,
	'utf8'
)
