//
// A simple tool to right shift all bytes in a file by 1.
// This is nessecary for the NES, which only has a 7-bit DAC. Giving it an 8-bit PCM stream will result in horrible clipping
//
// Use ffmpeg or similar tools to convert a file to a raw, unsigned 8-bit PCM stream.
//

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		printf("Usage: make7bit [FILENAME_IN] [FILENAME_OUT]\n");
		return 0;
	}

	FILE* f = fopen(argv[1], "rb");
	if(f == NULL)
	{
		printf("Could not open input file.\n");
		return 1;
	}
	fseek(f, 0, SEEK_END);
	long inSize = ftell(f);
	fseek(f, 0, SEEK_SET);

	char* data = malloc(inSize);
	size_t readSize = fread(data, inSize, 1, f);
	fclose(f);

	if(readSize != inSize)
	{
		printf("Failed to read entire file.\n");
		free(data);
		return 1;
	}

	for(long i = 0; i < inSize; i++)
		data[i] = (unsigned char)data[i] >> 1;

	f = fopen(argv[2], "wb");
	if(f == NULL)
	{
		printf("Could not open output file.\n");
		free(data);
		return 1;
	}
	fwrite(data, inSize, 1, f);
	fclose(f);

	free(data);
	return 0;
}
