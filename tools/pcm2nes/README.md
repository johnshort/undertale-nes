pcm2nes
=======

A simple tool to right shift all bytes in a file by 1.
This is nessecary for the NES, which only has a 7-bit DAC. Giving it an 8-bit PCM stream will result in horrible clipping

Converting
----------

Use ffmpeg or similar tools to convert a file to a raw, unsigned 8-bit PCM stream. Then simply do:

```
./pcm2nes 8bit_input.pcm 7bit_output.pcm
```

Using the supplied `convert.sh` script takes care of taming ffmpeg:

```
./convert.sh anything.mp3 7bit_output.pcm
```
