#!/bin/bash
if [ "$#" -ne 2 ]; then
    echo "Usage: convert.sh [INPUT] [OUTPUT]"
    exit
fi

INPUT_FILE=$1
OUTPUT_FILE=$2
INPUT_FILENAME=$(basename -- "$INPUT_FILE")
FILENAME="${INPUT_FILE%.*}"
TEMP_FILE="/tmp/stream.pcm"

ffmpeg -y -loglevel fatal -i $INPUT_FILE -f u8 -ac 1 -acodec pcm_u8 -ar 16000 $TEMP_FILE
./bin/pcm2nes $TEMP_FILE $OUTPUT_FILE
rm -f $TEMP_FILE
