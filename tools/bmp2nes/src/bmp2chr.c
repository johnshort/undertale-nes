//
//
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

typedef struct BitmapHeader
{
  uint32_t size;
  uint32_t width;
  uint32_t height;
  uint16_t planes;
  uint16_t bitCount;
  uint32_t compression;
  uint32_t sizeImage;
  uint32_t xPelsPerMeter;
  uint32_t yPelsPerMeter;
  uint32_t colorsUsed;
  uint32_t colorsImportant;
} BitmapHeader_t;

typedef struct Color
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} Color_t;

//
// Count all the colors in the file
//
int countColors(BitmapHeader_t* header, uint8_t* pixels)
{
	size_t size = header->width * header->height * (header->bitCount >> 3);
	uint32_t colors[256];
	int cIndex = 0;

	for(size_t i = 0; i < size; i += (header->bitCount >> 3))
	{
		uint32_t c = (pixels[i] << 16) | (pixels[i + 1] << 8) | pixels[i + 2];
		int found = 0;
		for(int j = 0; j < cIndex; j++)
		{
			if(colors[j] == c)
			{
				found = 1;
				break;
			}
		}
		if(!found)
		{
			if(cIndex == 256)
				return -1;
			colors[cIndex++] = c;
		}
	}

	return cIndex;
}

int makeTileset(BitmapHeader_t* header, uint8_t* pixels)
{
	return 0;
}

int bitmapIsSupported(BitmapHeader_t* header)
{
	if(header->size != 40)
	{
		printf("Unsupported DIB format %d\n", header->size);
		return 1;
	}

	if(header->compression != 0)
	{
		printf("Compression %d not supported\n", header->compression);
		return 1;
	}

	if(header->planes != 1)
	{
		printf("Only 1 color plane supported\n");
		return 1;
	}

	if(header->bitCount != 24)
	{
		printf("Only 24 bits per pixel supported\n");
		return 1;
	}

	if(
		((header->width  >> 3) << 3) != header->width ||
		((header->height >> 3) << 3) != header->height)
	{
		printf("Width and height must be divisable by 8\n");
		return 1;
	}
	return 0;
}

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		printf("Usage: bmp2chr [INPUT] [OUTPUT]\n");
		return 0;
	}

	char* inFile = argv[1];
	char* outFile = argv[2];

	// Open file... You all know this...
	FILE* f = fopen(inFile, "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	uint8_t* bmpFile = (uint8_t*)malloc(fsize + 1);
	fread(bmpFile, fsize, 1, f);
	fclose(f);

	// Magic number check
	if(bmpFile[0] != 'B' || bmpFile[1] != 'M')
	{
		printf("Invalid bitmap file\n");
		return 1;
	}

	uint32_t bmpSize = *(uint32_t*)&bmpFile[2];
	uint32_t pixelOffset = *(uint32_t*)&bmpFile[10];

	// This is why I love C
	BitmapHeader_t* header = (BitmapHeader_t*)&bmpFile[14];

	// Sanity checks
	if(bitmapIsSupported(header))
	{
		free(bmpFile);
		return 1;
	}

	// Color check
	int colors = countColors(header, &bmpFile[pixelOffset]);
	if(colors == -1)
	{
		printf("Bitmap contains more than 256 colors: We only support 4 colors.\n");
		free(bmpFile);
		return 1;
	}
	if(colors != 4)
	{
		printf("Bitmap contains %d colors: We only support 4 colors.\n", colors);
		free(bmpFile);
		return 1;
	}
	printf("Found %d colors\n", colors);

}
