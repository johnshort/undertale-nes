Tools
=====

This file gives a brief description of all the different tools we use, as well
as how to get them and install them.

nodejs
------

Node.js (or nodejs) is a Javascript runtime. We use this for writing small
scripts to aid us in development.

We use Javascript because it's quick and simple to use. We really don't need to
squeeze any performance out of our tools, so this is perfectly acceptable.

To install nodejs on Linux, simply type this command into a terminal:
```sh
apt install nodejs npm
```

On Windows and macOS, you can take the long route and download the installer
from `https://nodejs.org/en/download/`.


