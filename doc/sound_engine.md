Sound Engine
============

The creatively named Sound Engine is responsible for producing all audible
sounds in the game. This documents explains the internal operation.

Interface
---------

The external interface of Sound Engine is fairly simple. There's only 6 routines
you need to concern yourself with:
* SoundInit
* SoundLoadTrack
* SoundPlay
* SoundStop
* SoundPlaySoundEffect
* SoundUpdate

These should be fairly self-explanatory, but here's a one-liner of each of them:

#### SoundInit
Resets all nessecary memory of the engine. Call this one time as you initialize
other parts of your program.

#### SoundLoadTrack
Accepts a pointer to a sound track, where the upper byte of the address is
stored in the A register, and the lower byte of the address is stored in the X
register.
Calling this routine also resets the playback, so it's not nessecary to call
the initialization routine before calling this.

#### SoundPlay
Self-explanatory: Starts the playback.

#### SoundStop
Self-explanatory: Stops the playback.

#### SoundPlaySoundEffect
Accepts a pointer to a sound effect macro in the A and X registers, just like
the SoundLoadTrack routine. This pointer is written to the last macro slot.

#### SoundUpdate
This is where all the magic happens, and must be called once a frame whenever
there's any sound that needs to be played.

Memory
------

Sound Engine uses 2 kinds of memory:
* High priority memory
* Low priority memory

The high priority memory MUST be placed in zeropage. This section of memory
contains pointers, which are required to be placed in zeropage. Other memory is
placed here due to performance concerns.

Low priority memory is memory that's accessed infrequently, and thus there's not
really any performance issues with placing it anywhere. It contains mostly
pointers, but all of these are copied into the high priority memory before it's
used.


