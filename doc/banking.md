Banking
=======

This document describes the different banks we use.

For those unaware, a `bank` is simply a chunk of memory that can be swapped on
demand. For example, at memory address $8000, we have 2KiB of data we can
instantly swap with another 2KiB of data. These 2KiB swappable chunks of data
are known as a bank. It doesn't nessecarily have to be 2KiB, but you get the
idea.

Fixed banks
-----------

Located at address $E000, we have the last PRG bank defined in our ROM. It's
2KiB in size, and it cannot be swapped. It's always there. Because of this, we
should treat this bank as sacred memory, and we should only put code that
absolutely have to always be available on boot there.

At address $C000, we have our 2nd fixed 2KiB bank. Same rules as above applies,
except it's slightly less sacred. Non-critical code that benefits from always
being available should be placed here. Prefer this bank over $E000!


