Game engine design
==================

Each "screen" in the game has it's own scene. Each scene has a set of code
associated with it, which is responsible for playing out the scene, including
rendering the screen, selecting music and sound (which in turn are handled by
the Sound Engine) and processing input.

The list of scenes are as follows:

* Opening
* Main menu
* World
* Battle
* Dialouge

We divide the game into scenes, so that we can more easily compartmentalize the
game engine.

Scenes
------

Here is a description of each scene.

### Opening

The opening scene plays the intro music and opening animation. It's a very
simple scene.
The saved game stat can have an effect on the opening scene, but that is ignored
for the demo.

Any button input should skip the opening and jump straight into the main menu
scene.

### Main menu

The main menu scene is also very simple. When th user decides to make a new game
or continue, it will set the appropriate world variables and jump to the world
scene.

All sub menues and such are all handled by this scene.

### World

The world scene is very complex. It's responsible for loading and decoding the
appropriate maps, and rendering them to the screen.

### Battle

The battle scene is also very complex.

Map
---

The map, for now, is stored on a 32x32 tile basis.

There are multiple types of tiles:
* Background
* Solid
* Event



