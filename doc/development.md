Undertale NES Development Guidelines
====================================

Enviornment
-----------

The main bulk of development is done on Linux, and thus only Linux is fully
supported as a development enviornment. There's nothing preventing anyone from
developing on macOS or Windows, but it would require some modifications.

If anyone fancies to make the entire development enviornment portable to all
platforms, that would be much appreciated.

For Windows users, you can install the Windows Subsystem for Linux (WSL).

Banking
-------

In general, code should be placed in the switchable bank slot at $8000. Data
should be placed in the switchable bank at $A000.
If the routine has a very good reason to be placed at the $A000 bank, it must
clearly state so in the documentation, as special care has to be taken to jump
there.

See `banking.md` for a more detailed rundown.

General Guidelines
------------------

### 80 columns

All text files should to the best of their ability be limited to 80 columns
width. If the code is less readable as a result, it's allowed t exceed 80
columns.

### File extensions

Assembly sources should use te `.asm` extension.

### Documentation

All docs should be written in Markdown.

Assembly Code Guidelines
------------------------

### Label naming

Labels should use expressive names. The first letter of each word in the label
should be capitalized, including the first letter of the label.

This applies to both global and local labels, as well as labels pointing to data
and labels pointing to code.

Example:

```ca65
mylabel:  ; Incorrect: words not capitalized
myLabel:  ; Incorrect: all words should be capitalized
MYLABEL:  ; Incorrect: don't upcase the entire label
My_Label: ; Incorrect: don't use underscores
MyLabel:  ; Correct
```

### Indention

Indention must always use tabs. Do not use spaces.

In assembly, you should indent code between branches and their respective labels
by one tab if it makes sense. Let the branch instruction reside on the original
intention.

Sometimes, indenting the code between a branch and the corresponding labels
makes the code less readable, or it doesn't make sense to indent it. In those
cases it's acceptable to not indent the code, but as a general rule we should
try to make it obvious when a piece of code neatly belongs to a branch in a loop
or conditional.

Labels should always be placed on the very first column, unless they're local
labels or anonymous labels.

Exmaple:

```ca65
ClearZeropage: ; This is an entry label, and thus placed on the first column
	lda #0 ; Code between the entry label and the return should be indented
	tax
	:      ; This label is placed on the same intention as the branch it
	       ; belongs to
		sta $00,x ; Code between a branch and a label should be treated as a
		dex       ; scope and thus be indented.
	bne :- ; Branch insturctions break the scope
	rts
```

### Alignment

Tables and code that needs to be aligned should be alinged from the current
intention using spaces. Do not use tabs for indention, as it may cause the code
to go out of alignment if the tab size is changed.

Example:

```ca65
MyTable:
	;     size,  -- data -- , end
	.byte $03, $01, $01, $01, $00
	.byte $01, $01,           $00 ; Spaced used for aligning end bytes
```

### .proc directive

All callable routines should be surrounded with the `.proc ProcName` and
`.endproc` assembler directives.

### Routines and Registers

Unless otherwise specified, assume that all subroutines destroy the contents of
all registers. If a routine doesn't destroy all routines, but doesn't explicitly
say it saves them, don't assume a routine won't destroy all registers at a later
date. Be a defensive coder.

Javascript Code Guidelines
--------------------------

None. Write your tools however you like. 

Tools
-----

To make development easier, we have a set of standard tools we use. 

* nodejs - For scripting
* ca65 - Our assembler
* Famitracker - For creating sounds and music
* nesst - "NES Screen Tool", for creating palettes, nametables and tilemaps
* NintendulatorDX - Emulator with debug extensions

Anyone who wises to compile or contribute should have all of these tools
installed. See `tools.md` for more information on where to get them and how to
install them.
